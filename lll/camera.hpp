#pragma once

#include <glm/glm.hpp>
#include "plane.hpp"

struct camera{
    float vfov;
    float hfov;
    float near;
    float far;
    plane planes[6];
    glm::mat4 Projection;
    glm::mat4 View;
    //used for plane correction
    glm::mat4 transform;
    camera();
    //considering the instructions, do try to make them the same
    void setFov(float v, float h);
    void setNearFarPlane(float n, float f);
    void constructPlanes();
    void constructProjectionMatrix();
    void clipTest(float* v, float* ind);
    int clipVec(float* v, float* vec, float* ind);
    int clipLine(float* v1, float* v2);
    glm::vec3 getCameraAxis(int axis);
    void translate(const glm::vec3& vec);
    void rotate(int axis, float rad);

    glm::vec3 projectImage(const glm::vec3& position);
};