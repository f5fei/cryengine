#pragma once

#include <glm/glm.hpp>

//including this so its easier to debug for any file that inclues it
#include <iostream>
using namespace std;

void printVec(float* vec, int dim);

void printMat(float* mat, int dim);

void printVec3(glm::vec3 vec);

void printVec4(glm::vec4 vec);

void printMat4(glm::mat4 mat);