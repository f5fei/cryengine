#include "lllt.hpp"

#include <cmath>
#include <glm/gtc/type_ptr.hpp>

#include "debugging.hpp"

void getIdentityMatrix(float *mat) {
	for (int i = 0; i < 16; i++) {
		mat[i] = 0;
	}
	for (int i = 0; i < 4; i++) {
		mat[i * 5] = 1;
	}
}

glm::mat4 lllt::translate(const glm::vec3 &translation) {
	const float *ts = glm::value_ptr(translation);
	return glm::mat4(1, 0, 0, 0,
					 0, 1, 0, 0,
					 0, 0, 1, 0,
					 ts[0], ts[1], ts[2], 1);
}

/**
0 -> x
1 -> y
2 -> z
*/
glm::mat4 lllt::reflect(const int dim) {
	float mat[16];
	getIdentityMatrix(mat);
	//in a 4 by 4 matrix, 5 is the magic number for catching the diagonal line
	mat[dim * 5] *= -1;
	return glm::make_mat4(mat);
}

glm::mat4 rotaryEngine(const int axis, const float rsin, const float rcos) {
	float mat[16];
	getIdentityMatrix(mat);
	//we can super impose the relationship of any two coordinate into the relationship of x and y
	//the y represent the sin relation and the x the cosine, ensuring counter clockwise rotation
	int y, x;
	switch (axis){
		case 0:
			y = 2;
			x = 1;
			break;
		case 1:
			y = 0;
			x = 2;
			break;
		case 2:
			y = 1;
			x = 0;
			break;
		default:
			y = 1;
			x = 0;
			break;
	}

	//glm matrixes are column major
	//column represent the member of the vector it interacts with
	//row represent the output towards the vector
	mat[y * 4 + y] = rcos;
	mat[y * 4 + x] = -rsin;
	mat[x * 4 + y] = rsin;
	mat[x * 4 + x] = rcos;
	return glm::make_mat4(mat);
}

glm::mat4 lllt::rotate(const int axis, const float rad) {
	float rcos = cos(rad);
	float rsin = sin(rad);
	return rotaryEngine(axis, rsin, rcos);
}

glm::mat4 lllt::scale(const glm::vec3 &ss) {
	const float *sss = glm::value_ptr(ss);
	return glm::mat4(ss[0], 0, 0, 0,
					 0, ss[1], 0, 0,
					 0, 0, ss[2], 0,
					 0, 0, 0, 1);
}

//Bless Cormack
//Taken from fast inverse square root
float Q_rsqrt(const float number) {
	long i;
	float x2, y;
	const float threehalfs = 1.5F;

	x2 = number * 0.5F;
	y = number;
	i = *(long *)&y;		   // evil floating point bit level hacking
	i = 0x5f3759df - (i >> 1); // what the fuck?
	y = *(float *)&i;
	y = y * (threehalfs - (x2 * y * y)); // 1st iteration
	y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed
	return y;
}

glm::mat4 lllt::rotate(const glm::vec3 &position, const glm::vec3 &axis, const float rad) {
	float pco[3];
	const float *p = glm::value_ptr(position);
	for (int i = 0; i < 3; i++) {
		pco[i] = -p[i];
	}
	glm::mat4 preT = lllt::translate(glm::make_vec3(pco));
	glm::mat4 posT = lllt::translate(position);
	/*
	//vector snapping: Rotate such that the target rotation axis aligns with one of the canonical axis
	int targetAxis;
	const float *axi = glm::value_ptr(axis);
	
	float maxDim = 0;
	for(int i=0; i<3; i++){
		if(maxDim < axi[i]){
			maxDim = axi[i];
			targetAxis = i;
		}
	}
	
	glm::mat4 preSnap = glm::mat4(1);
	glm::mat4 postSnap = glm::mat4(1);
	for (int i = 0; i < 3; i++) {
		if (i != targetAxis && axi[i] != 0) {
			//i: The dimension to be reduced to 0 via rotation
			//targetAxis: The dimension to rotate to
			//a: the indicator, which axis to rotate from in order to reduce i
			//since theta is the angle between the current vector and the canonical axis projected to the a-plane, we can express cos and sin with i and targetAxis
			float root = axi[targetAxis] * axi[targetAxis] + axi[i] * axi[i];
			//float iroot = Q_rsqrt(root);
			root = sqrt(root);
			float rcos = axi[targetAxis] / root;
			float rsin = axi[i] / root;
			int a;
			for (a = 0; a == targetAxis || a == i; a++);
			glm::mat4 ccwise = rotaryEngine(a, rsin, rcos);
			glm::mat4 cwise = rotaryEngine(a, -rsin, rcos);
			if (targetAxis - i == 1 || targetAxis - i == -2) {
				preSnap = ccwise * preSnap;
				postSnap = postSnap * cwise;
			} else {
				preSnap = cwise * preSnap;
				postSnap = postSnap * ccwise;
			}
		}
	}
	glm::mat4 rotation = lllt::rotate(targetAxis, rad);*/
	/*cout<<"Rotation"<<endl;
	printMat4(rotation);
	cout<<"Snaps"<<endl;
	printMat4(preSnap);
	printMat4(postSnap);*/
	//return posT * postSnap * rotation * preSnap * preT;

	const float* axiv = glm::value_ptr(axis);
	float axii[3];
	float root;
	for(int i=0; i<3; i++){
		axii[i] = axiv[i];
		root += axii[i]*axii[i];
	}
	root = sqrt(root);
	for(int i=0; i<3; i++){
		axii[i]/root;
	}
	float rcos = cos(rad);
	float rsin = sin(rad);
	float dt = 1 - rcos;	

	float rot[16];
	const int x = 0;
	const int y = 1;
	const int z = 2;

	rot[ x*4 + x ] = rcos + dt * axii[x] * axii[x];
	rot[ x*4 + y ] = dt * axii[x] * axii[y] + rsin * axii[z];
	rot[ x*4 + z ] = dt * axii[x] * axii[z] - rsin * axii[y];
	rot[ x*4 + 3 ] = 0;

	rot[ y*4 + x ] = dt * axii[x] * axii[y] - rsin * axii[z];
	rot[ y*4 + y ] = rcos + dt * axii[y] * axii[y];
	rot[ y*4 + z ] = dt * axii[y] * axii[z] + rsin * axii[x];
	rot[ y*4 + 3 ] = 0;

	rot[ z*4 + x ] = dt * axii[z] * axii[x] + rsin * axii[y];
	rot[ z*4 + y ] = dt * axii[z] * axii[y] - rsin * axii[x];
	rot[ z*4 + z ] = rcos + dt * axii[z] * axii[z];
	rot[ z*4 + 3 ] = 0;

	rot[ 3*4 + x ] = 0;
	rot[ 3*4 + y ] = 0;
	rot[ 3*4 + z ] = 0;
	rot[ 3*4 + 3 ] = 1;

	glm::mat4 rotation = glm::make_mat4(rot);
	return posT * rotation * preT;
}

glm::mat4 lllt::scale(const glm::vec3 &position, const glm::vec3 &scales) {
	float pco[3];
	const float *p = glm::value_ptr(position);
	for (int i = 0; i < 3; i++) {
		pco[i] = -p[i];
	}
	glm::mat4 preT = lllt::translate(glm::make_vec3(pco));
	glm::mat4 posT = lllt::translate(position);
	glm::mat4 scaler = lllt::scale(scales);
	return posT * scaler * preT;
}

//Note: Projection matrix are incomplete due to the need for multiplication
glm::mat4 lllt::projection(const float near, const float far) {
	float p[16];
	getIdentityMatrix(p);
	float a, c;
	a = (far + near) / (far - near);
	c = 2 * far * near / (near - far);
	p[10] = a;
	p[11] = 1;
	p[14] = c;
	p[15] = 0;
	return glm::make_mat4(p);
}

//Note: As this relies on the ratio between feature and z value, this should be applied after projection
glm::mat4 lllt::fov(const float v, const float h) {
	float vrad = v / 2;
	float hrad = h / 2;
	float vrange = tan(vrad);
	float hrange = tan(hrad);
	float vmod = 1 / vrange;
	float hmod = 1 / hrange;
	return lllt::scale(glm::vec3(hmod, vmod, 1));
}

glm::mat4 lllt::perspective(const float n, const float f, const float v, const float h) {
	glm::mat4 proj = lllt::projection(n, f);
	glm::mat4 pv = lllt::fov(v, h);
	return pv * proj;
}
