#include "camera.hpp"
#include "lllt.hpp"
#include <glm/gtc/type_ptr.hpp>
#include "epsilon.hpp"
#include "debugging.hpp"

using namespace glm;
using namespace lllf;

camera::camera(){
    Projection = mat4(1);
    View = mat4(1);
    transform = mat4(1);
}

void camera::setFov(float v, float h){
    vfov = v;
    hfov = h;
}

void camera::setNearFarPlane(float n, float f){
    near = n;
    far = f;
}

void camera::constructPlanes(){
    /* vertice mapping:
    Within the quartile:
        counter clockwise
    Across the quartile:
        remainders are parallel
    */
    float canonicalPoints[8][3];

    float vr = tan(vfov/2);
    float hr = tan(hfov/2);

    //vr = 0.251;
    //hr = vr;

    int xmask[4] = {-1, 1, 1, -1};
    int ymask[4] = {-1, -1, 1, 1};
    float pl[2];
    pl[0] = near;
    pl[1] = far;

    for(int j=0; j<2; j++){
        for(int i=0; i<4; i++){
            canonicalPoints[j*4 + i][0] = xmask[i]*hr*pl[j];
            canonicalPoints[j*4 + i][1] = ymask[i]*vr*pl[j];
            canonicalPoints[j*4 + i][2] = pl[j];
        }
    }

    // for(int i=0; i<8; i++){
    //     printVec(canonicalPoints[i], 3);
    // }


    //transformations
    for(int i=0; i<8; i++){
        vec3 vertice = make_vec3(canonicalPoints[i]);
        vec4 position = vec4(vertice, 1);
        position = transform * position;
        float* v = value_ptr(position);
        for(int j=0; j<3; j++){
            canonicalPoints[i][j] = v[j];
        }
    }

    /*
    for(int i=0; i<8; i++){
        printVec(canonicalPoints[i], 3);
    }*/


    //graft points for plane creation
    float pts[9];
    //front
    pts[0] = canonicalPoints[0][0];
    pts[1] = canonicalPoints[0][1];
    pts[2] = canonicalPoints[0][2];
    pts[3] = canonicalPoints[3][0];
    pts[4] = canonicalPoints[3][1];
    pts[5] = canonicalPoints[3][2];
    pts[6] = canonicalPoints[1][0];
    pts[7] = canonicalPoints[1][1];
    pts[8] = canonicalPoints[1][2];
    planes[0].setAnchorPoints(pts);
    //left side
    pts[0] = canonicalPoints[0][0];
    pts[1] = canonicalPoints[0][1];
    pts[2] = canonicalPoints[0][2];
    pts[3] = canonicalPoints[4][0];
    pts[4] = canonicalPoints[4][1];
    pts[5] = canonicalPoints[4][2];
    pts[6] = canonicalPoints[3][0];
    pts[7] = canonicalPoints[3][1];
    pts[8] = canonicalPoints[3][2];
    planes[1].setAnchorPoints(pts);
    //bottom
    pts[0] = canonicalPoints[0][0];
    pts[1] = canonicalPoints[0][1];
    pts[2] = canonicalPoints[0][2];
    pts[3] = canonicalPoints[4][0];
    pts[4] = canonicalPoints[4][1];
    pts[5] = canonicalPoints[4][2];
    pts[6] = canonicalPoints[1][0];
    pts[7] = canonicalPoints[1][1];
    pts[8] = canonicalPoints[1][2];
    planes[2].setAnchorPoints(pts);
    //right side
    pts[0] = canonicalPoints[2][0];
    pts[1] = canonicalPoints[2][1];
    pts[2] = canonicalPoints[2][2];
    pts[3] = canonicalPoints[6][0];
    pts[4] = canonicalPoints[6][1];
    pts[5] = canonicalPoints[6][2];
    pts[6] = canonicalPoints[1][0];
    pts[7] = canonicalPoints[1][1];
    pts[8] = canonicalPoints[1][2];
    planes[3].setAnchorPoints(pts);
    //top
    pts[0] = canonicalPoints[2][0];
    pts[1] = canonicalPoints[2][1];
    pts[2] = canonicalPoints[2][2];
    pts[3] = canonicalPoints[6][0];
    pts[4] = canonicalPoints[6][1];
    pts[5] = canonicalPoints[6][2];
    pts[6] = canonicalPoints[3][0];
    pts[7] = canonicalPoints[3][1];
    pts[8] = canonicalPoints[3][2];
    planes[4].setAnchorPoints(pts);
    //back
    pts[0] = canonicalPoints[4][0];
    pts[1] = canonicalPoints[4][1];
    pts[2] = canonicalPoints[4][2];
    pts[3] = canonicalPoints[5][0];
    pts[4] = canonicalPoints[5][1];
    pts[5] = canonicalPoints[5][2];
    pts[6] = canonicalPoints[7][0];
    pts[7] = canonicalPoints[7][1];
    pts[8] = canonicalPoints[7][2];
    planes[5].setAnchorPoints(pts);

    //Not really a centoid per say, but guaranteed to be inside trivially, thus a good target for the test
    float centoid[3] = {0, 0, 0};

    centoid[2] = (near + far)/2;

    vec3 cec3 = make_vec3(centoid);
    vec4 cec = vec4(cec3, 1);    
    cec = transform * cec;
    float* f = value_ptr(cec);
    for(int i=0; i<3; i++){
        centoid[i] = f[i];
    }

    //printVec(centoid, 3);

    for(int i=0; i<6; i++){
        planes[i].computeNormal();
        float result = planes[i].inPlane(centoid);
        //Since we define the centoid to be inside, we will invert in case the result isnt desired
        if(result < 0){
            for(int j=0; j<3; j++){
                planes[i].normal[j] *= -1;
            }
        }
    }

    /*
    cout<<"Normal"<<endl;
    for(int i=0; i<6; i++){
        printVec(planes[i].normal, 3);
    }*/
}

void camera::constructProjectionMatrix(){
    Projection = lllt::perspective(near, far, vfov, hfov);
}

void camera::clipTest(float* v, float* ind){
    for(int i=0; i<6; i++){
        ind[i] = planes[i].inPlane(v);
    }
}

int camera::clipVec(float* v, float* vec, float* ind){
    float cli[3];
    float inc[6];
    for(int i=0; i<6; i++){
        if(ind[i] < 0){
            //construct a clipped candidate, test if it fits the other planes
            float ratio;
            float dot = 0;
            for(int j=0; j<3; j++){
                dot += vec[j] * planes[i].normal[j];
            }
            ratio = ind[i] / dot;
            for(int j=0; j<3; j++){
                cli[j] = v[j] - ratio * vec[j];
            }
            //evaluate the new clipped candidate
            //cout<<"Candidate Testing"<<endl;
            clipTest(cli, inc);
            bool fit = true;
            for(int j=0; j<6; j++){
                if(!signCheck(inc[j], 1)){
                    /*cout<<"This candidate died by "<<inc[j]<<endl;
                    cout<<"FYI, the candiate is "<<endl;
                    printVec(cli, 3);*/
                    fit = false;
                    break;
                }
            }
            //found a new point that fits
            if(fit){
                for(int j=0; j<3; j++){
                    v[j] = cli[j];
                }
                return 0;
            }
        }
    }
    //indicates utter failure at finding suitable candidate
    //this implies that the line segment cannot be drawn
    return 1;
}

//takes in the two point that forms a line, returns the two points modified (in case of clipping)
//number => number of lines cliped
//-1 => trivially rejected
//-2 => non-trivially rejected
int camera::clipLine(float* v1, float* v2){
    float in1[6];
    float in2[6];

    clipTest(v1, in1);
    clipTest(v2, in2);

    // printVec(v1, 3);
    // printVec(v2, 3);

    bool aClip = false;
    bool bClip = false;

    for(int i=0; i<6; i++){
        bool aOut = !signCheck(in1[i], 1);
        bool bOut = !signCheck(in2[i], 1);
        if(aOut){
            aClip = true;
            //cout<<"first point is out"<<endl;
        }
        if(bOut){
            bClip = true;
            //cout<<"second point is out"<<endl;
        }
        //trivial rejection
        if(aOut && bOut){
            return -1;
        }
    }

    if(!aClip && !bClip){
        return 0;
    }

    float vec[3];
    for(int i=0; i<3; i++){
        vec[i] = v1[i] - v2[i];
    }

    if(aClip && clipVec(v1, vec, in1)){
        return -2;
    }

    if(bClip && clipVec(v2, vec, in2)){
        return -2;
    }
    return aClip + bClip;
}

vec3 camera::getCameraAxis(int axis){
    float pos[4];
    for(int i=0; i<4; i++){
        pos[i] = 0;
    }
    pos[axis] = 1;
    vec4 position = make_vec4(pos);
    position = transform * position;
    return vec3(position);
}

void camera::translate(const vec3& vec){
    const float* fv = value_ptr(vec);
    float iv[3];
    for(int i=0; i<3; i++)
        iv[i] = -fv[i];
    vec3 inv = make_vec3(iv);
    mat4 norm = lllt::translate(vec);
    mat4 inve = lllt::translate(inv);
    //every operation applied to the camera must be applied inversely in view
    //relativity
    transform = norm * transform;
    View = View * inve;
}

void camera::rotate(int axis, float rad){
    vec3 axi = getCameraAxis(axis);
    vec3 pos = getCameraAxis(3);
    mat4 rt = lllt::rotate(pos, axi, rad);
    mat4 inv = lllt::rotate(pos, axi, -rad);
    transform = rt * transform;
    View = View * inv;
}

vec3 camera::projectImage(const vec3& position){
    vec4 vp = vec4(position, 1);
    vp = View * vp;
    vp = Projection * vp;
    float *f = value_ptr(vp);
    float nf[3];
    for(int i=0; i<3; i++){
        nf[i] = f[i]/f[3];
    }
    return make_vec3(nf);
}
