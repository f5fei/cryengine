#pragma once

/*
Lennox's Library of Linear Transformations

A basic library of operations for the most elemental operations in graphics

Basic conventions:

Always follows the precedence order of x,y,z
Degrees are measured in radians
All rotations are performed counter clock wise -> please keep that in mind

*/

#include <glm/glm.hpp>

namespace lllt{
    
    glm::mat4 translate(const glm::vec3& translation);

    glm::mat4 reflect(const int dim);

    glm::mat4 rotate(const int axis, const float rad);

    glm::mat4 scale(const glm::vec3& ss);

    
    glm::mat4 rotate(const glm::vec3& position, const glm::vec3& axis, const float rad);

    glm::mat4 scale(const glm::vec3& position, const glm::vec3& scales);


    glm::mat4 projection(const float near, const float far);
    
    glm::mat4 fov(const float v, const float h);
    
    glm::mat4 perspective(const float n, const float f, const float v, const float h);
};
