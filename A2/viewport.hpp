#pragma once

#include <glm/glm.hpp>

struct viewPort{
    double lx, ly, ux, uy;
    glm::mat3 norm;
    void buildNorm();
    float flx, fly, fux, fuy;
};