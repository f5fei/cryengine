#pragma once

#include <glm/glm.hpp>


struct mesh{
    bool cleanup;
    int verticeCount;
    float* vertices;
    //object model transformations
    //we will assume that initial position is 0,0,0
    glm::mat4 transform;
    mesh();
    void setVertice(int verticeCount, float* vertices, bool cleanup = false);
    glm::vec3 getPosition();
    //xyz -> 012
    glm::vec3 getModelAxis(int axis);
    void translate(glm::vec3 trs);
    void scale(glm::vec3 sc);
    void rotate(int axis, float rad);
    ~mesh();
};