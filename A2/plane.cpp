#include <cmath>
#include "plane.hpp"

plane::plane(){

}

void plane::setAnchor(const float* anchor){
    for(int i=0; i<3; i++){
        this->anchor[i] = anchor[i];
    }
}

void plane::setBasis(const float* basis){
    for(int i=0; i<6; i++){
        this->basis[i/3][i%3] = basis[i];
    }
}

void plane::setAnchorPoints(const float* anchors){
    for(int i=0; i<3; i++){
        anchor[i] = anchors[i];
    }
    for(int i=3; i<9; i++){
        basis[i/3 - 1][i%3] = anchors[i] - anchors[i - 3];
    }
}

//IE -> find the cross product of the two basis
void plane::computeNormal(){
    normal[0] = basis[0][1]*basis[1][2] - basis[0][2]*basis[1][1];
    normal[1] = basis[0][2]*basis[1][0] - basis[0][0]*basis[1][2];
    normal[2] = basis[0][0]*basis[1][1] - basis[0][1]*basis[1][0];

    float abs = 0;
    for(int i=0; i<3; i++){
        abs += normal[i]*normal[i];
    }
    abs = sqrt(abs);
    for(int i=0; i<3; i++){
        normal[i] /= abs;
    }
}

void plane::setInPlane(const float* vertice){
	if(inPlane(vertice)) return;
	for(int i=0; i<3; i++){
		normal[i] *= -1;
	}
}

float plane::inPlane(const float* point){
    float vector[3];
    for(int i=0; i<3; i++){
        vector[i] = point[i] - anchor[i];
    }
    float dot = 0;
    for(int i=0; i<3; i++){
        dot += vector[i] * normal[i];
    }
    return dot;
}
