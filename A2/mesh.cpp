#include "mesh.hpp"
#include "lllt.hpp"
#include <glm/gtc/type_ptr.hpp>

using namespace lllt;
using namespace glm;

mesh::mesh(){
    transform = mat4(1);
}

void mesh::setVertice(int verticeCount, float* vertices, bool cleanup){
    this->verticeCount = verticeCount;
    this->vertices = vertices;
    this->cleanup = cleanup;
}

vec3 mesh::getPosition(){
    return getModelAxis(3);
}

vec3 mesh::getModelAxis(int axis){
    float vector[4] = {0, 0, 0, 0};
    vector[axis] = 1;
    return vec3(transform*make_vec4(vector));
}


void mesh::translate(vec3 trs){
    //todo: remove scale from vectors
    vec3 axis[3];
    for(int i=0; i<3; i++){
        axis[i] = getModelAxis(i);
    }
    float *f = value_ptr(trs);

    vec3 ccc = vec3(0);
    for(int i=0; i<3; i++){
        ccc = ccc + axis[i] * f[i];
    }

    mat4 norm = lllt::translate(ccc);
    transform = norm * transform;
}

void mesh::scale(vec3 sc){
    mat4 norm = lllt::scale(getPosition(), sc);
    transform = norm * transform;
}
void mesh::rotate(int axis, float rad){
    vec3 axi = getModelAxis(axis);
    vec3 pos = getModelAxis(3);
    mat4 rt = lllt::rotate(pos, axi, rad);
    transform = rt * transform;
}

mesh::~mesh(){
    if(cleanup)
        delete [] vertices;
}