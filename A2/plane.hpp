#pragma once

struct plane{
    float anchor[3];
    float basis[2][3];
    float normal[3];

    plane();
    void setAnchor(const float* anchor);
    void setBasis(const float* basis);
    void setAnchorPoints(const float* anchors);
    void computeNormal();
	void setInPlane(const float* vertice);
    float inPlane(const float* point);
};
