#include "viewport.hpp"

using namespace glm;

void viewPort::buildNorm(){
    float width = 768;
    float height = 768;
    //shuffle the data
    double llx = lx;
    lx = lx < ux ? lx : ux;
    ux = llx < ux ? ux : llx;
    llx = ly;
    ly = ly < uy ? ly : uy;
    uy = llx < uy ? uy : llx;

    flx = lx/width*2 - 1;
    fly = ly/height*2 - 1;

    fux = ux/width*2 - 1;
    fuy = uy/height*2 - 1;

    float rx = (ux - lx)/width;
    float ry = (uy - ly)/height;

    //objective: -1, -1 => lx, ly
    //             1, 1 => ux, uy

    mat3 translate_to_origin = mat3( 1, 0, 0,
                                     0, 1, 0,
                                     1, 1, 1);
    mat3 scalar = mat3( rx, 0, 0,
                        0, ry, 0,
                        0, 0, 1);

    mat3 translate_to_lxy = mat3( 1, 0, 0,
                                  0, 1, 0,
                                  flx, fly, 1);

    norm = translate_to_lxy * scalar * translate_to_origin;
}