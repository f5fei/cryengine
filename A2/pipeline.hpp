#pragma once

struct lineBatch{
    int lineCount;
    float* vertices;
};

class mesh;
class camera;

lineBatch pipeline(mesh& mes, camera& cam);