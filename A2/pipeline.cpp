#include "pipeline.hpp"
#include "camera.hpp"
#include "mesh.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
using namespace std;

#include "debugging.hpp"

using namespace glm;

//assumes that we are drawing the cube, with expected 12 lines
lineBatch pipeline(mesh& mes, camera& cam){
    //cout<<"---------------"<<endl;
    lineBatch batch;
    float vertices[24];
    for(int i=0; i<mes.verticeCount; i++){
        vec3 opos = make_vec3(mes.vertices + i*3);
        vec4 fpos = vec4(opos, 1);
        fpos = mes.transform * fpos;
        float* p = value_ptr(fpos);
        for(int j=0; j<3; j++){
            vertices[i*3 + j] = p[j];
        }
    }
    int pairs[24];

    //the 4 lines in the front
    for(int i=0; i<4; i++){
        pairs[2*i] = i;
        pairs[2*i + 1] = i+1;
    }
    pairs[7] = 0;

    //in the middle
    for(int i=4; i<8; i++){
        pairs[2*i] = i - 4;
        pairs[2*i+1] = i;
    }

    //the 4 lines in the back
    for(int i = 8; i<12; i++){
        pairs[2*i] = i-4;
        pairs[2*i+1] = i-4+1;
    }
    pairs[23] = 4;

    cam.constructPlanes();

    //pair wise processing
    //it's possible to end up with less, but this will guarantee no realloc 
    float* vss = new float[12*2*3];
    float vbuffer1[3];
    float vbuffer2[3];
    int numpair = 0;
    for(int i=0; i<12; i++){
        int t = pairs[i*2];
        for(int j=0; j<3; j++){
            vbuffer1[j] = vertices[t*3 + j];
        }
        t = pairs[i*2 + 1];
        for(int j=0; j<3; j++){
            vbuffer2[j] = vertices[t*3 + j];
        }
        // printVec(vbuffer1, 3);
        // printVec(vbuffer2, 3);
        int status = cam.clipLine(vbuffer1, vbuffer2);
        // cout<<"Vertice "<<pairs[i*2]<<"\t"<<pairs[i*2+1]<<endl;
        // cout<<"Clipping status: "<<status<<endl;
        // printVec(vbuffer1, 3);
        // printVec(vbuffer2, 3);
        if(status>=0){
            for(int i=0; i<3; i++){
                vss[numpair*6+i] = vbuffer1[i];
                vss[numpair*6+3+i] = vbuffer2[i];
            }
            // printVec(vss + numpair*6, 3);
            // printVec(vss + numpair*6 + 3, 3);
            numpair++;
        } else {
            //cout<<"Clipping rejected: "<<status<<endl;
        }
        batch.vertices = vss;
        batch.lineCount = numpair;
    }
    //cout<<"---------------"<<endl;
    return batch;
}