/*
Lennox's Library of Linear Mathematical Operations
*/
#pragma once

namespace lllm{
	void crossProduct(float* v1, float* v2, float* v3);
}
