// Winter 2020

#include "A2.hpp"
#include "cs488-framework/GlErrorCheck.hpp"

#include <iostream>
using namespace std;

#include <imgui/imgui.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>
using namespace glm;

#include "pipeline.hpp"
#include "debugging.hpp"
#include <cmath>

const float pie = 3.1415926;

//----------------------------------------------------------------------------------------
// Constructor
VertexData::VertexData()
	: numVertices(0),
	  index(0)
{
	positions.resize(kMaxVertices);
	colours.resize(kMaxVertices);
}


//----------------------------------------------------------------------------------------
// Constructor
A2::A2()
	: m_currentLineColour(vec3(0.0f))
{
	float cubic[24] = {	-1, -1, -1,
					     1, -1, -1,
						 1,  1, -1,
						-1,  1, -1,
						-1, -1,  1,
						 1, -1,  1,
						 1,  1,  1,
						-1,  1,  1 };
	float* cubes = new float[8*3];
	for(int i=0; i<24; i++){
		cubes[i] = cubic[i];
	}
	cube.setVertice(8, cubes, true);
	//per request, 30 degree fov
	cam.setFov(pie*30/180, pie*30/180);
	cam.setNearFarPlane(1.0, 25.0);
	cam.constructProjectionMatrix();
	cam.translate(vec3(0, 0, -5));
	p.lx = 20;
	p.ly = 20;
	p.ux = 748;
	p.uy = 748;
	p.buildNorm();
	//cam.rotate(2, pie/4);
	//cam.rotate(1, pie/24);
	//cam.rotate(0, pie/24);
	//printVec3(cam.getCameraAxis(2));
}

//----------------------------------------------------------------------------------------
// Destructor
A2::~A2()
{

}

//----------------------------------------------------------------------------------------
/*
 * Called once, at program start.
 */
void A2::init()
{
	// Set the background colour.
	glClearColor(0.3, 0.5, 0.7, 1.0);

	createShaderProgram();

	glGenVertexArrays(1, &m_vao);

	enableVertexAttribIndices();

	generateVertexBuffers();

	mapVboDataToVertexAttributeLocation();
}

//----------------------------------------------------------------------------------------
void A2::createShaderProgram()
{
	m_shader.generateProgramObject();
	m_shader.attachVertexShader( getAssetFilePath("VertexShader.vs").c_str() );
	m_shader.attachFragmentShader( getAssetFilePath("FragmentShader.fs").c_str() );
	m_shader.link();
}

//---------------------------------------------------------------------------------------- Winter 2020
void A2::enableVertexAttribIndices()
{
	glBindVertexArray(m_vao);

	// Enable the attribute index location for "position" when rendering.
	GLint positionAttribLocation = m_shader.getAttribLocation( "position" );
	glEnableVertexAttribArray(positionAttribLocation);

	// Enable the attribute index location for "colour" when rendering.
	GLint colourAttribLocation = m_shader.getAttribLocation( "colour" );
	glEnableVertexAttribArray(colourAttribLocation);

	// Restore defaults
	glBindVertexArray(0);

	CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
void A2::generateVertexBuffers()
{
	// Generate a vertex buffer to store line vertex positions
	{
		glGenBuffers(1, &m_vbo_positions);

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_positions);

		// Set to GL_DYNAMIC_DRAW because the data store will be modified frequently.
		glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * kMaxVertices, nullptr,
				GL_DYNAMIC_DRAW);


		// Unbind the target GL_ARRAY_BUFFER, now that we are finished using it.
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		CHECK_GL_ERRORS;
	}

	// Generate a vertex buffer to store line colors
	{
		glGenBuffers(1, &m_vbo_colours);

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_colours);

		// Set to GL_DYNAMIC_DRAW because the data store will be modified frequently.
		glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * kMaxVertices, nullptr,
				GL_DYNAMIC_DRAW);


		// Unbind the target GL_ARRAY_BUFFER, now that we are finished using it.
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		CHECK_GL_ERRORS;
	}
}

//----------------------------------------------------------------------------------------
void A2::mapVboDataToVertexAttributeLocation()
{
	// Bind VAO in order to record the data mapping.
	glBindVertexArray(m_vao);

	// Tell GL how to map data from the vertex buffer "m_vbo_positions" into the
	// "position" vertex attribute index for any bound shader program.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_positions);
	GLint positionAttribLocation = m_shader.getAttribLocation( "position" );
	glVertexAttribPointer(positionAttribLocation, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	// Tell GL how to map data from the vertex buffer "m_vbo_colours" into the
	// "colour" vertex attribute index for any bound shader program.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_colours);
	GLint colorAttribLocation = m_shader.getAttribLocation( "colour" );
	glVertexAttribPointer(colorAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	//-- Unbind target, and restore default values:
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	CHECK_GL_ERRORS;
}

//---------------------------------------------------------------------------------------
void A2::initLineData()
{
	m_vertexData.numVertices = 0;
	m_vertexData.index = 0;
}

//---------------------------------------------------------------------------------------
void A2::setLineColour (
		const glm::vec3 & colour
) {
	m_currentLineColour = colour;
}

//---------------------------------------------------------------------------------------
void A2::drawLine(
		const glm::vec2 & V0,   // Line Start (NDC coordinate)
		const glm::vec2 & V1    // Line End (NDC coordinate)
) {

	m_vertexData.positions[m_vertexData.index] = V0;
	m_vertexData.colours[m_vertexData.index] = m_currentLineColour;
	++m_vertexData.index;
	m_vertexData.positions[m_vertexData.index] = V1;
	m_vertexData.colours[m_vertexData.index] = m_currentLineColour;
	++m_vertexData.index;

	m_vertexData.numVertices += 2;
}

//------------------------------------------------------------------------------
void A2::reset(){
	cam.setFov(30*pie/180, 30*pie/180);
	cam.setNearFarPlane(1, 25);
	cam.constructProjectionMatrix();
	cam.transform = mat4(1);
	cam.View = mat4(1);
	cam.translate(vec3(0, 0, -5));
	cube.transform = mat4(1);
	p.lx = 20;
	p.ly = 20;
	p.ux = 748;
	p.uy = 748;
	p.buildNorm();

}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, before guiLogic().
 */
void A2::appLogic()
{
	// Place per frame, application logic here ...

	// Call at the beginning of frame, before drawing lines:
	initLineData();

	lineBatch batch = pipeline(cube, cam);
	setLineColour(vec3(1,1,1));

	float v1[3];
	float v2[3];

	float vi1[2];
	float vi2[2];

	//draws the vertices around the base cube
	for(int i=0; i<batch.lineCount; i++){
		for(int j=0; j<3; j++){
			v1[j] = batch.vertices[i*6+j];
		}
		for(int j=0; j<3; j++){
			v2[j] = batch.vertices[i*6+j+3];
		}
		
		/*cout<<"Line"<<i<<endl;

        printVec(v1, 3);
        printVec(v2, 3);
		*/
		vec3 vc1 = make_vec3(v1);
		vec3 vc2 = make_vec3(v2);

		vec4 vp1 = vec4(vc1, 1);
		vec4 vp2 = vec4(vc2, 1);

		float* f = value_ptr(vp1);
		vp1 = cam.View * vp1;
		vp1 = cam.Projection * vp1;
		vi1[0] = f[0]/f[3];
		vi1[1] = f[1]/f[3];
		vec2 vl1 = make_vec2(vi1);
		//printVec(f, 3);

		vp2 = cam.View * vp2;
		vp2 = cam.Projection * vp2;
		f = value_ptr(vp2);
		vi2[0] = f[0]/f[3];
		vi2[1] = f[1]/f[3];
		vec2 vl2 = make_vec2(vi2);
		//printVec(f, 3);
		vl1 = vec2(p.norm * vec3(vl1, 1));
		vl2 = vec2(p.norm * vec3(vl2, 1));

		drawLine(vl1, vl2);
	}

	//draws the canonical (wcs) axis
	float canonicalAnchor[4][3];
	for(int i=0; i<4; i++){
		for(int j=0; j<3; j++){
			canonicalAnchor[i][j] = 0;
		}
	}
	for(int i=1; i<4; i++){
		canonicalAnchor[i][i-1] = 1;
	}

	for(int i=1; i<4; i++){
		float v1[3], v2[3];
		for(int j=0; j<3; j++){
			v1[j] = canonicalAnchor[0][j];
			v2[j] = canonicalAnchor[i][j];
		}
		int status = cam.clipLine(v1, v2);
		if(status >= 0){
			float fg[3] = {0.5,0.5,0.5};
			fg[i-1] = 1;
			setLineColour(make_vec3(fg));
			vec2 f1 = vec2(cam.projectImage(make_vec3(v1)));
			vec2 f2 = vec2(cam.projectImage(make_vec3(v2)));
			f1 = vec2(p.norm * vec3(f1, 1));
			f2 = vec2(p.norm * vec3(f2, 1));
			drawLine(f1, f2);
		}
	}

	//draws the model axis
	for(int i=1; i<4; i++){
		canonicalAnchor[i][i-1] = 0.5;
	}

	
	for(int i=1; i<4; i++){
		float v1[3], v2[3];
		for(int j=0; j<3; j++){
			v1[j] = canonicalAnchor[0][j];
			v2[j] = canonicalAnchor[i][j];
		}
		vec4 p1 = cube.transform * vec4(make_vec3(v1), 1);
		vec4 p2 = cube.transform * vec4(make_vec3(v2), 1);
		float *ff1 = value_ptr(p1);
		float *ff2 = value_ptr(p2);
		for(int j=0; j<3; j++){
			v1[j] = ff1[j];
			v2[j] = ff2[j];
		}
		int status = cam.clipLine(v1, v2);
		if(status >= 0){
			float fg[3] = {0.5,0.5,0.5};
			fg[i-1] = 1;
			setLineColour(make_vec3(fg));
			vec2 f1 = vec2(cam.projectImage(make_vec3(v1)));
			vec2 f2 = vec2(cam.projectImage(make_vec3(v2)));
			f1 = vec2(p.norm * vec3(f1, 1));
			f2 = vec2(p.norm * vec3(f2, 1));
			drawLine(f1, f2);
		}
	}

	//how to clip


	setLineColour(vec3(0.2,0.2,0.2));
	drawLine(vec2(p.flx, p.fly), vec2(p.fux, p.fly));
	drawLine(vec2(p.flx, p.fly), vec2(p.flx, p.fuy));
	drawLine(vec2(p.fux, p.fuy), vec2(p.fux, p.fly));
	drawLine(vec2(p.fux, p.fuy), vec2(p.flx, p.fuy));

	/*
	// Draw outer square:
	setLineColour(vec3(1.0f, 0.7f, 0.8f));
	drawLine(vec2(-0.5f, -0.5f), vec2(0.5f, -0.5f));
	drawLine(vec2(0.5f, -0.5f), vec2(0.5f, 0.5f));
	drawLine(vec2(0.5f, 0.5f), vec2(-0.5f, 0.5f));
	drawLine(vec2(-0.5f, 0.5f), vec2(-0.5f, -0.5f));


	// Draw inner square:
	setLineColour(vec3(0.2f, 1.0f, 1.0f));
	drawLine(vec2(-0.25f, -0.25f), vec2(0.25f, -0.25f));
	drawLine(vec2(0.25f, -0.25f), vec2(0.25f, 0.25f));
	drawLine(vec2(0.25f, 0.25f), vec2(-0.25f, 0.25f));
	drawLine(vec2(-0.25f, 0.25f), vec2(-0.25f, -0.25f));
	*/

}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after appLogic(), but before the draw() method.
 */
void A2::guiLogic()
{
	static bool firstRun(true);
	if (firstRun) {
		ImGui::SetNextWindowPos(ImVec2(50, 50));
		firstRun = false;
	}

	static bool showDebugWindow(true);
	ImGuiWindowFlags windowFlags(ImGuiWindowFlags_AlwaysAutoResize);
	float opacity(0.5f);

	ImGui::Begin("Properties", &showDebugWindow, ImVec2(100,100), opacity,
			windowFlags);


		// Add more gui elements here here ...
		if( ImGui::RadioButton( "Rotate View", &interactionMode, 0 ) ) {
			
		}
		ImGui::SameLine();
		if( ImGui::RadioButton( "Translate View", &interactionMode, 1 ) ) {

		}
		ImGui::SameLine();
		if( ImGui::RadioButton( "Perspective View", &interactionMode, 2 ) ) {

		}
		if( ImGui::RadioButton( "Rotate Model", &interactionMode, 3 ) ) {

		}
		ImGui::SameLine();
		if( ImGui::RadioButton( "Translate Model", &interactionMode, 4 ) ) {

		}
		ImGui::SameLine();
		if( ImGui::RadioButton( "Scale Model", &interactionMode, 5 ) ) {

		}
		if( ImGui::RadioButton( "Viewport", &interactionMode, 6) ){

		}

		// Create Button, and check if it was clicked:
		if( ImGui::Button( "Quit Application" ) ) {
			glfwSetWindowShouldClose(m_window, GL_TRUE);
		}

		ImGui::Text( "Framerate: %.1f FPS", ImGui::GetIO().Framerate );

	ImGui::End();
}

//----------------------------------------------------------------------------------------
void A2::uploadVertexDataToVbos() {

	//-- Copy vertex position data into VBO, m_vbo_positions:
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_positions);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec2) * m_vertexData.numVertices,
				m_vertexData.positions.data());
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		CHECK_GL_ERRORS;
	}

	//-- Copy vertex colour data into VBO, m_vbo_colours:
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_colours);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec3) * m_vertexData.numVertices,
				m_vertexData.colours.data());
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		CHECK_GL_ERRORS;
	}
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after guiLogic().
 */
void A2::draw()
{
	uploadVertexDataToVbos();

	glBindVertexArray(m_vao);

	m_shader.enable();
		glDrawArrays(GL_LINES, 0, m_vertexData.numVertices);
	m_shader.disable();

	// Restore defaults
	glBindVertexArray(0);

	CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
/*
 * Called once, after program is signaled to terminate.
 */
void A2::cleanup()
{

}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles cursor entering the window area events.
 */
bool A2::cursorEnterWindowEvent (
		int entered
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}


//-----------------------------------------------------------------------------------------
/*
 * Process mouse movement
 */
void A2::rotateView(double x, double y){
	float dx = x - aX;
	float dy = y - aY;
	for(int i=0; i<3; i++){
		if(mouseDown[i]){
			cam.rotate(i, dx / 1024);
		}
	}
}

//-----------------------------------------------------------------------------------------
/*
 * Process mouse movement
 */
void A2::translateView(double x, double y){
	float dx = x - aX;
	float dy = y - aY;
	float vv[3];
	for(int i=0; i<3; i++){
		if(mouseDown[i]){
			vv[i] = dx/1024;
		} else {
			vv[i] = 0;
		}
	}
	cam.translate(make_vec3(vv));
}

//-----------------------------------------------------------------------------------------
/*
 * Process mouse movement
 */
void A2::perspective(double x, double y){
	float dx = x - aX;
	float dy = y - aY;
	if(mouseDown[0]){
		fov = fov*(1 + dx/1024);
		if(fov > 160){
			fov = 160;
		}
		if(fov < 5){
			fov = 5;
		}
		cam.setFov(fov*pie/180, fov*pie/180);
	}
	if(mouseDown[1]){
		cam.near += dx/256;
	}
	if(mouseDown[2]){
		cam.far += dx/256;
	}
	cam.constructProjectionMatrix();
}

//-----------------------------------------------------------------------------------------
/*
 * Process mouse movement
 */
void A2::rotateModel(double x, double y){
	float dx = x - aX;
	float dy = y - aY;
	for(int i=0; i<3; i++){
		if(mouseDown[i]){
			cube.rotate(i, dx / 1024);
		}
	}
}

//-----------------------------------------------------------------------------------------
/*
 * Process mouse movement
 */
void A2::translateModel(double x, double y){
	float dx = x - aX;
	float dy = y - aY;
	float vv[3];
	for(int i=0; i<3; i++){
		if(mouseDown[i]){
			vv[i] = dx/1024;
		} else {
			vv[i] = 0;
		}
	}
	cube.translate(make_vec3(vv));
}

//-----------------------------------------------------------------------------------------
/*
 * Process mouse movement
 */
void A2::scaleModel(double x, double y){
	float dx = x - aX;
	float dy = y - aY;
	float vv[3];
	for(int i=0; i<3; i++){
		if(mouseDown[i]){
			vv[i] = 1+dx/1024;
		} else {
			vv[i] = 1;
		}
	}
	cube.scale(make_vec3(vv));
}

//-----------------------------------------------------------------------------------------
/*
 * Process mouse movement
 */
void A2::viewport(double x, double y){

	if(mouseClicked[0]){
		p.lx = x;
		p.ly = y;
	}
	if(mouseReleased[0]){
		p.ux = x;
		p.uy = y;
		p.buildNorm();
	}

}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse cursor movement events.
 */
bool A2::mouseMoveEvent (
		double xPos,
		double yPos
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	if (!ImGui::IsMouseHoveringAnyWindow()) {
		switch (interactionMode){
			case 0:
				rotateView(xPos, yPos);
				break;
			case 1:
				translateView(xPos, yPos);
				break;
			case 2:
				perspective(xPos, yPos);
				break;
			case 3:
				rotateModel(xPos, yPos);
				break;
			case 4:
				translateModel(xPos, yPos);
				break;
			case 5:
				scaleModel(xPos, yPos);
				break;
			case 6:
				viewport(xPos, yPos);
				break;
		}
		aX = xPos;
		aY = yPos;
		for(int i=0; i<3; i++){
			if(mouseClicked[i]){
				mouseClicked[i] = 0;
			}

			if(mouseReleased[i]){
				mouseReleased[i] = 0;
			}
		}
	}

	eventHandled = true;

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse button events.
 */
bool A2::mouseButtonInputEvent (
		int button,
		int actions,
		int mods
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	if (!ImGui::IsMouseHoveringAnyWindow()) {
	
		int index = 0;

		switch (button){
			case GLFW_MOUSE_BUTTON_LEFT:
				index = 0;
				break;
			case GLFW_MOUSE_BUTTON_MIDDLE:
				index = 1;
				break;
			case GLFW_MOUSE_BUTTON_RIGHT:
				index = 2;
				break;
			default:
				index = 0;
				break;
		}
	
		if(actions == GLFW_PRESS){
			mouseClicked[index] = 1;
			mouseDown[index] = 1;
		} else if (actions == GLFW_RELEASE){
			mouseDown[index] = 0;
			mouseReleased[index] = 1;
		}
	}

	eventHandled = true;

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse scroll wheel events.
 */
bool A2::mouseScrollEvent (
		double xOffSet,
		double yOffSet
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles window resize events.
 */
bool A2::windowResizeEvent (
		int width,
		int height
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles key input events.
 */
bool A2::keyInputEvent (
		int key,
		int action,
		int mods
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}
