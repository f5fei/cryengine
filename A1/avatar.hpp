//So the cube were a tragic case of bad abstraction
//lets try to make it better this time eh?

#include "instance.hpp"

//unfortunately, the instance and the model will always require strong coupling due to the shaders
class sphere;

class avatar : public Instance {
    friend class sphere;
    public:
    avatar(sphere& m, glm::mat4& transformation, glm::vec4& color);
    ~avatar();
    private:
    glm::vec4 color;
};