#include "sphere.hpp"
#include "avatar.hpp"
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
using namespace std;

//Reference for the "starter" vertice and polygon bindings
//http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
void starterIcosahedron(float* vertices, int* polygons, int stride){

    float t = (sqrt(5.0f) + 1.0f) / 2.0f;
    vertices[ 4 * 0 + 0 ] = -1;
    vertices[ 4 * 0 + 1 ] = t;
    vertices[ 4 * 0 + 2 ] = 0;
    vertices[ 4 * 0 + 3 ] = 0;
    vertices[ 4 * 1 + 0 ] = 1;
    vertices[ 4 * 1 + 1 ] = t;
    vertices[ 4 * 1 + 2 ] = 0;
    vertices[ 4 * 1 + 3 ] = 0;
    vertices[ 4 * 2 + 0 ] = -1;
    vertices[ 4 * 2 + 1 ] = -t;
    vertices[ 4 * 2 + 2 ] = 0;
    vertices[ 4 * 2 + 3 ] = 0;
    vertices[ 4 * 3 + 0 ] = 1;
    vertices[ 4 * 3 + 1 ] = -t;
    vertices[ 4 * 3 + 2 ] = 0;
    vertices[ 4 * 3 + 3 ] = 0;
    vertices[ 4 * 4 + 0 ] = 0;
    vertices[ 4 * 4 + 1 ] = -1;
    vertices[ 4 * 4 + 2 ] = t;
    vertices[ 4 * 4 + 3 ] = 0;
    vertices[ 4 * 5 + 0 ] = 0;
    vertices[ 4 * 5 + 1 ] = 1;
    vertices[ 4 * 5 + 2 ] = t;
    vertices[ 4 * 5 + 3 ] = 0;
    vertices[ 4 * 6 + 0 ] = 0;
    vertices[ 4 * 6 + 1 ] = -1;
    vertices[ 4 * 6 + 2 ] = -t;
    vertices[ 4 * 6 + 3 ] = 0;
    vertices[ 4 * 7 + 0 ] = 0;
    vertices[ 4 * 7 + 1 ] = 1;
    vertices[ 4 * 7 + 2 ] = -t;
    vertices[ 4 * 7 + 3 ] = 0;
    vertices[ 4 * 8 + 0 ] = t;
    vertices[ 4 * 8 + 1 ] = 0;
    vertices[ 4 * 8 + 2 ] = -1;
    vertices[ 4 * 8 + 3 ] = 0;
    vertices[ 4 * 9 + 0 ] = t;
    vertices[ 4 * 9 + 1 ] = 0;
    vertices[ 4 * 9 + 2 ] = 1;
    vertices[ 4 * 9 + 3 ] = 0;
    vertices[ 4 * 10 + 0 ] = -t;
    vertices[ 4 * 10 + 1 ] = 0;
    vertices[ 4 * 10 + 2 ] = -1;
    vertices[ 4 * 10 + 3 ] = 0;
    vertices[ 4 * 11 + 0 ] = -t;
    vertices[ 4 * 11 + 1 ] = 0;
    vertices[ 4 * 11 + 2 ] = 1;
    vertices[ 4 * 11 + 3 ] = 0;

    //populate the polygons with massive gaps based on stride
    //this way, each time, tessellation can be done on the local subarray with the identical pattern

    polygons[ 3 * stride * 0 + 0 ] = 0;
    polygons[ 3 * stride * 0 + 1 ] = 11;
    polygons[ 3 * stride * 0 + 2 ] = 5;
    polygons[ 3 * stride * 1 + 0 ] = 0;
    polygons[ 3 * stride * 1 + 1 ] = 5;
    polygons[ 3 * stride * 1 + 2 ] = 1;
    polygons[ 3 * stride * 2 + 0 ] = 0;
    polygons[ 3 * stride * 2 + 1 ] = 1;
    polygons[ 3 * stride * 2 + 2 ] = 7;
    polygons[ 3 * stride * 3 + 0 ] = 0;
    polygons[ 3 * stride * 3 + 1 ] = 7;
    polygons[ 3 * stride * 3 + 2 ] = 10;
    polygons[ 3 * stride * 4 + 0 ] = 0;
    polygons[ 3 * stride * 4 + 1 ] = 10;
    polygons[ 3 * stride * 4 + 2 ] = 11;
    polygons[ 3 * stride * 5 + 0 ] = 1;
    polygons[ 3 * stride * 5 + 1 ] = 5;
    polygons[ 3 * stride * 5 + 2 ] = 9;
    polygons[ 3 * stride * 6 + 0 ] = 5;
    polygons[ 3 * stride * 6 + 1 ] = 11;
    polygons[ 3 * stride * 6 + 2 ] = 4;
    polygons[ 3 * stride * 7 + 0 ] = 11;
    polygons[ 3 * stride * 7 + 1 ] = 10;
    polygons[ 3 * stride * 7 + 2 ] = 2;
    polygons[ 3 * stride * 8 + 0 ] = 10;
    polygons[ 3 * stride * 8 + 1 ] = 7;
    polygons[ 3 * stride * 8 + 2 ] = 6;
    polygons[ 3 * stride * 9 + 0 ] = 7;
    polygons[ 3 * stride * 9 + 1 ] = 1;
    polygons[ 3 * stride * 9 + 2 ] = 8;
    polygons[ 3 * stride * 10 + 0 ] = 3;
    polygons[ 3 * stride * 10 + 1 ] = 9;
    polygons[ 3 * stride * 10 + 2 ] = 4;
    polygons[ 3 * stride * 11 + 0 ] = 3;
    polygons[ 3 * stride * 11 + 1 ] = 4;
    polygons[ 3 * stride * 11 + 2 ] = 2;
    polygons[ 3 * stride * 12 + 0 ] = 3;
    polygons[ 3 * stride * 12 + 1 ] = 2;
    polygons[ 3 * stride * 12 + 2 ] = 6;
    polygons[ 3 * stride * 13 + 0 ] = 3;
    polygons[ 3 * stride * 13 + 1 ] = 6;
    polygons[ 3 * stride * 13 + 2 ] = 8;
    polygons[ 3 * stride * 14 + 0 ] = 3;
    polygons[ 3 * stride * 14 + 1 ] = 8;
    polygons[ 3 * stride * 14 + 2 ] = 9;
    polygons[ 3 * stride * 15 + 0 ] = 4;
    polygons[ 3 * stride * 15 + 1 ] = 9;
    polygons[ 3 * stride * 15 + 2 ] = 5;
    polygons[ 3 * stride * 16 + 0 ] = 2;
    polygons[ 3 * stride * 16 + 1 ] = 4;
    polygons[ 3 * stride * 16 + 2 ] = 11;
    polygons[ 3 * stride * 17 + 0 ] = 6;
    polygons[ 3 * stride * 17 + 1 ] = 2;
    polygons[ 3 * stride * 17 + 2 ] = 10;
    polygons[ 3 * stride * 18 + 0 ] = 8;
    polygons[ 3 * stride * 18 + 1 ] = 6;
    polygons[ 3 * stride * 18 + 2 ] = 7;
    polygons[ 3 * stride * 19 + 0 ] = 9;
    polygons[ 3 * stride * 19 + 1 ] = 8;
    polygons[ 3 * stride * 19 + 2 ] = 1;

    for(int i=0; i<12; i++)
        cout<<vertices[i*4]<<"\t"<<vertices[i*4 + 1]<<"\t"<<vertices[i*4 + 2]<<"\t"<<vertices[i*4 + 3]<<"\t"<<endl;

}

void tessellationSquare(float* vertices, int* polygons, int& vId, int polyIndex, int stride){
    int a = polygons[ 3 * polyIndex * stride + 0 ];
    int b = polygons[ 3 * polyIndex * stride + 1 ];
    int c = polygons[ 3 * polyIndex * stride + 2 ];
    int ab = vId ++;
    int bc = vId ++;
    int ca = vId ++;
    
    float r = vertices[ 4*a + 0 ]*vertices[ 4*a + 0 ] + vertices[ 4*a + 1 ]*vertices[ 4*a + 1 ] 
    + vertices[ 4*a + 2 ]*vertices[ 4*a + 2 ] + vertices[ 4*a + 3 ]*vertices[ 4*a + 3 ];
    r = std::sqrt(r);

    //construct the mid points
    vertices[ 4*ab + 0 ] = (vertices[ 4*a + 0 ] + vertices[ 4*b + 0 ]) / 2.0f;
    vertices[ 4*ab + 1 ] = (vertices[ 4*a + 1 ] + vertices[ 4*b + 1 ]) / 2.0f;
    vertices[ 4*ab + 2 ] = (vertices[ 4*a + 2 ] + vertices[ 4*b + 2 ]) / 2.0f;
    vertices[ 4*ab + 3 ] = (vertices[ 4*a + 3 ] + vertices[ 4*b + 3 ]) / 2.0f;
    vertices[ 4*bc + 0 ] = (vertices[ 4*b + 0 ] + vertices[ 4*c + 0 ]) / 2.0f;
    vertices[ 4*bc + 1 ] = (vertices[ 4*b + 1 ] + vertices[ 4*c + 1 ]) / 2.0f;
    vertices[ 4*bc + 2 ] = (vertices[ 4*b + 2 ] + vertices[ 4*c + 2 ]) / 2.0f;
    vertices[ 4*bc + 3 ] = (vertices[ 4*b + 3 ] + vertices[ 4*c + 3 ]) / 2.0f;
    vertices[ 4*ca + 0 ] = (vertices[ 4*c + 0 ] + vertices[ 4*a + 0 ]) / 2.0f;
    vertices[ 4*ca + 1 ] = (vertices[ 4*c + 1 ] + vertices[ 4*a + 1 ]) / 2.0f;
    vertices[ 4*ca + 2 ] = (vertices[ 4*c + 2 ] + vertices[ 4*a + 2 ]) / 2.0f;
    vertices[ 4*ca + 3 ] = (vertices[ 4*c + 3 ] + vertices[ 4*a + 3 ]) / 2.0f;

    int vertexScalar[3];
    vertexScalar[0] = ab;
    vertexScalar[1] = bc;
    vertexScalar[2] = ca;


    for(int i=0; i<3; i++){
        int n = vertexScalar[i];
        float rn = vertices[ 4*n + 0 ] * vertices[ 4*n + 0 ] + vertices[ 4*n + 1 ] * vertices[ 4*n + 1 ]
        + vertices[ 4*n + 2 ] * vertices[ 4*n + 2 ] + vertices[ 4*n + 3 ] * vertices[ 4*n + 3 ];
        rn = sqrt(rn);
        float scale = r / rn;
        vertices[4*n + 0]*=scale;
        vertices[4*n + 1]*=scale;
        vertices[4*n + 2]*=scale;
        vertices[4*n + 3]*=scale;
    }

    int substride = stride/4;
    polygons[ 3 * (polyIndex * stride + substride * 0) + 0 ] = a;
    polygons[ 3 * (polyIndex * stride + substride * 0) + 1 ] = ab;
    polygons[ 3 * (polyIndex * stride + substride * 0) + 2 ] = ca;
    polygons[ 3 * (polyIndex * stride + substride * 1) + 0 ] = ca;
    polygons[ 3 * (polyIndex * stride + substride * 1) + 1 ] = ab;
    polygons[ 3 * (polyIndex * stride + substride * 1) + 2 ] = bc;
    polygons[ 3 * (polyIndex * stride + substride * 2) + 0 ] = b;
    polygons[ 3 * (polyIndex * stride + substride * 2) + 1 ] = bc;
    polygons[ 3 * (polyIndex * stride + substride * 2) + 2 ] = ab;
    polygons[ 3 * (polyIndex * stride + substride * 3) + 0 ] = c;
    polygons[ 3 * (polyIndex * stride + substride * 3) + 1 ] = ca;
    polygons[ 3 * (polyIndex * stride + substride * 3) + 2 ] = ab;

}


sphere* sphere::tessellateSpheres(int factor, ShaderProgram& shader){
    int baseVerticeCount = 12;
    int basePolygonCount = 20;
    int tessellatedVerticeCount = baseVerticeCount;
    int tessellatedPolygonCount = basePolygonCount;
    int multiplier = 1;
    for(int i=0; i<factor; i++){
        tessellatedVerticeCount*=2;
        tessellatedPolygonCount*=4;
        multiplier*=4;
    }
    float* vertices = new float[4*tessellatedVerticeCount];
    int* polygons = new int[3*tessellatedPolygonCount];

    starterIcosahedron(vertices, polygons, multiplier);

    //the next vertice will have an id of 12
    int vId = 12;

    int polyCount = 20;

    for(int i = 1; i<factor; i++){
        for(int j=0; j<polyCount; j++){
            tessellationSquare(vertices, polygons, vId, j, multiplier);
        }
        multiplier/=4;
        polyCount*=4;
    }

    return new sphere(vertices, polygons, vId, polyCount, shader);
}


sphere::sphere(float* mesh, int* indice, int verticeCount, int polyCount, ShaderProgram& shader):Model(mesh, indice, verticeCount, polyCount, shader){
    P = shader.getUniformLocation( "P" );
    V = shader.getUniformLocation( "V" );
}

void sphere::configureShader(){
    
    glBindVertexArray(VAO);
    
    GLint vAtr = shader.getAttribLocation("vx");    
    GLint cAtr = shader.getAttribLocation("colour");
    GLint mAtr = shader.getAttribLocation("M");

    glBindBuffer( GL_ARRAY_BUFFER, modelVBO );
    glEnableVertexAttribArray( vAtr );
    glVertexAttribPointer ( vAtr, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), nullptr );
    
    glBindBuffer( GL_ARRAY_BUFFER, instanceVBO );
    glEnableVertexAttribArray(cAtr);
    glVertexAttribPointer ( cAtr, 4, GL_FLOAT, GL_FALSE, 20*sizeof(float), nullptr );
    //packing a transform matrix is more costly
    glEnableVertexAttribArray(mAtr + 0);
    glVertexAttribPointer(mAtr + 0, 4, GL_FLOAT, GL_FALSE, 20*sizeof(float), (void*)(4*sizeof(float)));
    glEnableVertexAttribArray(mAtr + 1);
    glVertexAttribPointer(mAtr + 1, 4, GL_FLOAT, GL_FALSE, 20*sizeof(float), (void*)(8*sizeof(float)));
    glEnableVertexAttribArray(mAtr + 2);
    glVertexAttribPointer(mAtr + 2, 4, GL_FLOAT, GL_FALSE, 20*sizeof(float), (void*)(12*sizeof(float)));
    glEnableVertexAttribArray(mAtr + 3);
    glVertexAttribPointer(mAtr + 3, 4, GL_FLOAT, GL_FALSE, 20*sizeof(float), (void*)(16*sizeof(float)));

    glBindVertexArray(0);
    glBindBuffer( GL_ARRAY_BUFFER, 0);

}

void sphere::update(){
    float* instanceBuffer = new float[instanceCount * 20];
    for(int i=0; i<instanceCount; i++){
        avatar* instance = (avatar*)instances[i];
        instanceBuffer[i*20 + 0] = instance->color[0];
        instanceBuffer[i*20 + 1] = instance->color[1];
        instanceBuffer[i*20 + 2] = instance->color[2];
        instanceBuffer[i*20 + 3] = instance->color[3];

        const float* m = (const float*)glm::value_ptr(instance->transformation);

        instanceBuffer[i*20 + 4] = m[0];
        instanceBuffer[i*20 + 5] = m[1];
        instanceBuffer[i*20 + 6] = m[2];
        instanceBuffer[i*20 + 7] = m[3];
        instanceBuffer[i*20 + 8] = m[4];
        instanceBuffer[i*20 + 9] = m[5];
        instanceBuffer[i*20 + 10] = m[6];
        instanceBuffer[i*20 + 11] = m[7];
        instanceBuffer[i*20 + 12] = m[8];
        instanceBuffer[i*20 + 13] = m[9];
        instanceBuffer[i*20 + 14] = m[10];
        instanceBuffer[i*20 + 15] = m[11];
        instanceBuffer[i*20 + 16] = m[12];
        instanceBuffer[i*20 + 17] = m[13];
        instanceBuffer[i*20 + 18] = m[14];
        instanceBuffer[i*20 + 19] = m[15];
    }
    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
    glBufferData(GL_ARRAY_BUFFER, 20*sizeof(float)*instanceCount, instanceBuffer, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    delete[] instanceBuffer;

}

void sphere::updateUniform(glm::mat4 proj, glm::mat4 view){

    shader.enable();

	glUniformMatrix4fv( P, 1, GL_FALSE, glm::value_ptr( proj ) );
	glUniformMatrix4fv( V, 1, GL_FALSE, glm::value_ptr( view ) );

    shader.disable();
}

sphere::~sphere(){
}