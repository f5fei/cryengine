#include "model.hpp"

//debugging purpose, plz remove
#include <iostream>
using namespace std;

Model::Model(float* mesh, int* indice, int verticeCount, int polyCount, ShaderProgram& shader)
: shader(shader), indice(indice), verticeCount(verticeCount), polyCount(polyCount), instanceCount(0) {

    cout<<"Model constructor sanity check:"<<endl;
    for(int i=0; i<verticeCount; i++){
        cout<<mesh[4*i + 0]<<","<<mesh[4*i + 1]<<","<<mesh[4*i + 2]<<","<<mesh[4*i + 3]<<","<<endl;
    }

    glGenVertexArrays(1, &VAO);
    glBindVertexArray( VAO );
    glGenBuffers(1, &modelVBO);
    glBindBuffer(GL_ARRAY_BUFFER, modelVBO);
    glBufferData(GL_ARRAY_BUFFER, verticeCount*4*sizeof(float), mesh, GL_STREAM_DRAW);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //nothing to do here
    glGenBuffers(1, &instanceVBO);
    delete [] mesh;
}

int Model::getId() const{
    return id;
}

//since uniform update requires shader.enable, remember to do that in the prep phase
void Model::drawInstances(){
    shader.enable();
    
    glBindVertexArray( VAO );

    cout<<"VAO"<<VAO<<endl;
    
    cout<<"Drawing " << instanceCount << " instances, polyCount: "<< polyCount << endl;

    glDrawElementsInstanced(GL_TRIANGLES, polyCount*3, GL_UNSIGNED_INT, indice, instanceCount);

    glBindVertexArray( 0 );

    shader.disable();
}

int Model::registerInstance(Instance& instance){
    instances.push_back(&instance);
    return ++instanceCount;
}

Model::~Model(){
    delete [] indice;
    glDeleteBuffers(1, &modelVBO);
    glDeleteBuffers(1, &instanceVBO);
    glDeleteVertexArrays(1, &VAO);
}