// Winter 2020

#include "A1.hpp"
#include "cs488-framework/GlErrorCheck.hpp"

#include <iostream>

#include <sys/types.h>
#include <unistd.h>

#include <imgui/imgui.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


using namespace glm;
using namespace std;

static const size_t DIM = 16;

//----------------------------------------------------------------------------------------
// Constructor
A1::A1()
	: current_col( 0 ), heightModifier( 0 ), updateMaze{true}, clicked(false), interrupted(true), released(false)
{
	colour[0] = 0.0f;
	colour[1] = 0.0f;
	colour[2] = 0.0f;
}

//----------------------------------------------------------------------------------------
// Destructor
A1::~A1()
{
	delete [] heightMap;
	delete [] positions;
	delete [] instancePayload;
	delete prototype;
	delete player;
}

//----------------------------------------------------------------------------------------
/*
 * Called once, at program start.
 */
void A1::init()
{

	// Initialize random number generator
	int rseed=getpid();
	srandom(rseed);
	// Print random number seed in case we want to rerun with
	// same random numbers
	cout << "Random number seed = " << rseed << endl;
	

	// DELETE FROM HERE...
	Maze m(DIM);
	m.digMaze();
	m.printMaze();
	// ...TO HERE

	generateHeightMap(m);

	// Set the background colour.
	glClearColor( 0.3, 0.5, 0.7, 1.0 );

	// Build the shader
	m_shader.generateProgramObject();
	m_shader.attachVertexShader(
		getAssetFilePath( "VertexShader.vs" ).c_str() );
	m_shader.attachFragmentShader(
		getAssetFilePath( "FragmentShader.fs" ).c_str() );
	m_shader.link();

	// Set up the uniforms
	P_uni = m_shader.getUniformLocation( "P" );
	V_uni = m_shader.getUniformLocation( "V" );
	M_uni = m_shader.getUniformLocation( "M" );
	col_uni = m_shader.getUniformLocation( "colour" );

	compileCubeShader();
	cubeShaderConfig();
	
	initAvatar();

	configureAvatarShader();

	initGrid();

	// Set up initial view and projection matrices (need to do this here,
	// since it depends on the GLFW window being set up correctly).
	view = glm::lookAt( 
		glm::vec3( 0.0f, 2.*float(DIM)*2.0*M_SQRT1_2, float(DIM)*2.0*M_SQRT1_2 ),
		glm::vec3( 0.0f, 0.0f, 0.0f ),
		glm::vec3( 0.0f, 1.0f, 0.0f ) );

	proj = glm::perspective( 
		glm::radians( 30.0f ),
		float( m_framebufferWidth ) / float( m_framebufferHeight ),
		1.0f, 1000.0f );
	
	world = glm::translate( world, vec3( -float(DIM)/2.0f, 0, -float(DIM)/2.0f ) );

}



void A1::configureAvatarShader(){
	float* vs = new float[48];
	int* is = new int[20*3];
	starterIcosahedron(vs, is, 1);

	sp_shader.generateProgramObject();	
	sp_shader.attachVertexShader(
		getAssetFilePath( "sp.vs" ).c_str() );
	sp_shader.attachFragmentShader(
		getAssetFilePath( "sp.fs" ).c_str() );
	sp_shader.link();


	P_sp = m_shader.getUniformLocation( "P" );
	V_sp = m_shader.getUniformLocation( "V" );
	M_sp = m_shader.getUniformLocation( "M" );
	col_sp = m_shader.getUniformLocation( "colour" );

	glGenBuffers( 1, &modelVBO );
	glGenVertexArrays( 1, &spVAO );
	glBindVertexArray(spVAO);
	glBindBuffer( GL_ARRAY_BUFFER, modelVBO);
	glBufferData( GL_ARRAY_BUFFER, sizeof(float)*48, vs, GL_STATIC_DRAW );
	GLint vsAttr = sp_shader.getAttribLocation("vx");
	glEnableVertexAttribArray( vsAttr );
	glVertexAttribPointer( vsAttr, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), nullptr );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindVertexArray( 0 );

	spIndex = is;
}

void A1::initGrid()
{
	size_t sz = 3 * 2 * 2 * (DIM+3);

	float *verts = new float[ sz ];
	size_t ct = 0;
	for( int idx = 0; idx < DIM+3; ++idx ) {
		verts[ ct ] = -1;
		verts[ ct+1 ] = 0;
		verts[ ct+2 ] = idx-1;
		verts[ ct+3 ] = DIM+1;
		verts[ ct+4 ] = 0;
		verts[ ct+5 ] = idx-1;
		ct += 6;

		verts[ ct ] = idx-1;
		verts[ ct+1 ] = 0;
		verts[ ct+2 ] = -1;
		verts[ ct+3 ] = idx-1;
		verts[ ct+4 ] = 0;
		verts[ ct+5 ] = DIM+1;
		ct += 6;
	}

	// Create the vertex array to record buffer assignments.
	glGenVertexArrays( 1, &m_grid_vao );
	glBindVertexArray( m_grid_vao );

	// Create the cube vertex buffer
	glGenBuffers( 1, &m_grid_vbo );
	glBindBuffer( GL_ARRAY_BUFFER, m_grid_vbo );
	glBufferData( GL_ARRAY_BUFFER, sz*sizeof(float),
		verts, GL_STATIC_DRAW );

	// Specify the means of extracting the position values properly.
	GLint posAttrib = m_shader.getAttribLocation( "position" );
	glEnableVertexAttribArray( posAttrib );
	glVertexAttribPointer( posAttrib, 3, GL_FLOAT, GL_FALSE, 0, nullptr );

	// Reset state to prevent rogue code from messing with *my* 
	// stuff!
	glBindVertexArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	// OpenGL has the buffer now, there's no need for us to keep a copy.
	delete [] verts;

	CHECK_GL_ERRORS;
}


//--------------------------------------------------------------------------------------
/* 
 * taking the data out of maze into A1
 */
void A1::generateHeightMap(Maze& m){

	heightMap = new float[DIM*DIM];
	for(int x=0; x<DIM; x++){
		for(int y=0; y<DIM; y++){
			heightMap[ x + y*DIM ] = m.getValue(x, y);
		}
	}

	//generating the positions
	//we are going to start the points at 0,0,0, in keeping with the grind line code
	positions = new float[3*DIM*DIM];
	for(int i=0; i<DIM*DIM; i++){
		//from the grid layout to the physical layout
		int x = i%DIM;
		int y = i/DIM;
		// cout<<"x:"<<x<<"\ty:"<<y<<"\t"<<endl;
		//negative adjustments due to the curious location of 0,0,0
		positions[3*i+0] = x;
		positions[3*i+1] = 0;
		positions[3*i+2] = y;
	}
	// printMap();
}

//--------------------------------------------------------------------------------------
/* 
 * debugging
 */
void A1::printMap(){

	//le debugging run
	//heightmap
	for(int x=0; x<DIM; x++){
		for(int y=0; y<DIM; y++){
			cout<<heightMap[ x + y*DIM ]<<" ";
		}
		cout<<endl;
	}

	//positions
	for(int x=0; x<DIM; x++){
		for(int y=0; y<DIM; y++){
			int i = x + y*DIM;
			cout<<positions[3*i]<<","<<positions[3*i+1]<<","<<positions[3*i+2]<<" ";
		}
		cout<<endl;
	}
}


//--------------------------------------------------------------------------------------
/* 
 * hooking up the cube_shader with its dedicated shader and uniform attributes
 */
void A1::compileCubeShader(){

	//Build the cube shader
	cube_shader.generateProgramObject();
	cube_shader.attachVertexShader(
		getAssetFilePath( "VSCube.vs" ).c_str()
	);
	cube_shader.attachFragmentShader(
		getAssetFilePath( "FSCube.fs" ).c_str()
	);
	cube_shader.link();

	// Set up the uniforms
	P_unic = cube_shader.getUniformLocation( "P" );
	V_unic = cube_shader.getUniformLocation( "V" );
	M_unic = cube_shader.getUniformLocation( "M" );
	col_unic = cube_shader.getUniformLocation( "colour" );
	H_unic = cube_shader.getUniformLocation( "H" );
}

//--------------------------------------------------------------------------------------
/* 
 * uploading the data and configuring the parameters
 */
void A1::cubeShaderConfig(){
	//binds the vao we are using for the cube object
	glGenVertexArrays( 1, &m_cube_vao );
	glBindVertexArray( m_cube_vao );
	
	//as the model is a static object with its own arrangement, it gets its own vbo
	glGenBuffers(1, &m_model_vbo);
	glBindBuffer( GL_ARRAY_BUFFER, m_model_vbo);
	glBufferData( GL_ARRAY_BUFFER, 8*3*sizeof(float), cube_mesh, GL_STATIC_DRAW );

	//set the vertex data
	GLint vAttrib = cube_shader.getAttribLocation("vertex");
	glEnableVertexAttribArray( vAttrib );
	glVertexAttribPointer( vAttrib, 3, GL_FLOAT, GL_FALSE, 0, nullptr );
	
	instancePayload = new float[DIM*DIM*4];

	//generate the buffer for instanced model
	//as this is actually more dynamic, we expect this to not persist
	glGenBuffers(1, &m_instance_vbo);
	glBindBuffer( GL_ARRAY_BUFFER, m_instance_vbo );
	// glBufferData( GL_ARRAY_BUFFER, 4*DIM*DIM*sizeof(float), instancePayload, GL_DYNAMIC_DRAW);

	GLint pAttrib = cube_shader.getAttribLocation("pos");
	GLint hAttrib = cube_shader.getAttribLocation("height");
	glEnableVertexAttribArray( pAttrib );
	glVertexAttribPointer( pAttrib, 3, GL_FLOAT, GL_FALSE, 4*sizeof(float), nullptr );
	glEnableVertexAttribArray( hAttrib );
	glVertexAttribPointer( hAttrib, 1, GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)(3*sizeof(float)) );
	glVertexAttribDivisor(pAttrib, 1);
	glVertexAttribDivisor(hAttrib, 1);


	//unload the model buffer
	glBindBuffer( GL_ARRAY_BUFFER, 0);

	//unload the array object
	glBindVertexArray( 0 );
}


//-------------------------------------------------------------------------------------
/*
 * Used to regenerate the walls data, if there has been a change occurring
 */
void A1::computeCubeInstances(){
	//updates the list of cubes that can be drawn

	updateMaze = false;

	numOfInstance = 0;

	for(int i=0; i<DIM*DIM; i++){
		if(heightMap[i] > 0.25f){
			int j = numOfInstance++;
			instancePayload[4*j + 0] = positions[3*i + 0];
			instancePayload[4*j + 1] = positions[3*i + 1];
			instancePayload[4*j + 2] = positions[3*i + 2];
			instancePayload[4*j + 3] = heightMap[i];
		}
	}
	
	glBindBuffer( GL_ARRAY_BUFFER, m_instance_vbo );
	glBufferData( GL_ARRAY_BUFFER, 4*numOfInstance*sizeof(float), instancePayload, GL_DYNAMIC_DRAW);
	glBindBuffer( GL_ARRAY_BUFFER, m_instance_vbo );

}

//------------------------------------------------------------------------------------------
/*
 * initialize the avatar mesh object
 */

void A1::initAvatar(){

	//Build the cube shader
	airbender.generateProgramObject();
	airbender.attachVertexShader(
		getAssetFilePath( "sphere.vs" ).c_str()
	);
	airbender.attachFragmentShader(
		getAssetFilePath( "sphere.fs" ).c_str()
	);
	airbender.link();

	prototype = sphere::tessellateSpheres(1, airbender);
	prototype->configureShader();
	//TODO: implement translation
	glm::mat4 m = glm::translate( glm::mat4(), vec3( -float(DIM)/2.0f, 0, -float(DIM)/2.0f ) );
	glm::vec4 c = glm::vec4( glm::vec3( 1.0f ), 0.5f );
	player = new avatar(*prototype, m, c);

	cubic.generateProgramObject();
	cubic.attachVertexShader(
		getAssetFilePath( "cube.vs" ).c_str()
	);
	cubic.attachFragmentShader(
		getAssetFilePath( "cube.fs" ).c_str()
	);
	cubic.link();

	protocube = getNewCube(cubic);
	protocube->configureShader();
	ccc = new Instance(*protocube, m);
}



//----------------------------------------------------------------------------------------
/*
 * Called once per frame, before guiLogic().
 */
void A1::appLogic()
{
	// Place per frame, application logic here ...

	if(updateMaze){
		computeCubeInstances();
	}

	if(!interrupted){
		world = glm::translate( world, vec3( float(DIM)/2.0f, 0, float(DIM)/2.0f ) );
		world = glm::rotate(world, radian, glm::vec3(0,1,0));
		world = glm::translate( world, vec3( -float(DIM)/2.0f, 0, -float(DIM)/2.0f ) );
	}

}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after appLogic(), but before the draw() method.
 */
void A1::guiLogic()
{	
	// We already know there's only going to be one window, so for 
	// simplicity we'll store button states in static local variables.
	// If there was ever a possibility of having multiple instances of
	// A1 running simultaneously, this would break; you'd want to make
	// this into instance fields of A1.
	static bool showTestWindow(false);
	static bool showDebugWindow(true);

	ImGuiWindowFlags windowFlags(ImGuiWindowFlags_AlwaysAutoResize);
	float opacity(0.5f);

	ImGui::Begin("Debug Window", &showDebugWindow, ImVec2(100,100), opacity, windowFlags);
		if( ImGui::Button( "Quit Application" ) ) {
			glfwSetWindowShouldClose(m_window, GL_TRUE);
		}

		// Eventually you'll create multiple colour widgets with
		// radio buttons.  If you use PushID/PopID to give them all
		// unique IDs, then ImGui will be able to keep them separate.
		// This is unnecessary with a single colour selector and
		// radio button, but I'm leaving it in as an example.

		// Prefixing a widget name with "##" keeps it from being
		// displayed.

		ImGui::PushID( 0 );
		ImGui::ColorEdit3( "##Colour", colour );
		ImGui::SameLine();
		if( ImGui::RadioButton( "##Col", &current_col, 0 ) ) {
			// Select this colour.
		}
		ImGui::PopID();

/*
		// For convenience, you can uncomment this to show ImGui's massive
		// demonstration window right in your application.  Very handy for
		// browsing around to get the widget you want.  Then look in 
		// shared/imgui/imgui_demo.cpp to see how it's done.
		if( ImGui::Button( "Test Window" ) ) {
			showTestWindow = !showTestWindow;
		}
*/

		ImGui::Text( "Framerate: %.1f FPS", ImGui::GetIO().Framerate );

	ImGui::End();

	if( showTestWindow ) {
		ImGui::ShowTestWindow( &showTestWindow );
	}
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after guiLogic().
 */
void A1::draw()
{
	// Create a global transformation for the model (centre it).

	m_shader.enable();
		glEnable( GL_DEPTH_TEST );

		glUniformMatrix4fv( P_uni, 1, GL_FALSE, value_ptr( proj ) );
		glUniformMatrix4fv( V_uni, 1, GL_FALSE, value_ptr( view ) );
		glUniformMatrix4fv( M_uni, 1, GL_FALSE, value_ptr( world ) );

		// Just draw the grid for now.
		glBindVertexArray( m_grid_vao );
		glUniform3f( col_uni, 1, 1, 1 );
		glDrawArrays( GL_LINES, 0, (3+DIM)*4 );

	m_shader.disable();

	//
	cube_shader.enable();

		glUniformMatrix4fv( P_unic, 1, GL_FALSE, value_ptr( proj ) );
		glUniformMatrix4fv( V_unic, 1, GL_FALSE, value_ptr( view ) );
		glUniformMatrix4fv( M_unic, 1, GL_FALSE, value_ptr( world ) );
		glUniform1f( H_unic, heightModifier );
		glUniform3f( col_unic, colour[0], colour[1], colour[2] );

		glBindVertexArray(m_cube_vao);

		// Draw the cubes
		// Highlight the active square.
		glDrawElementsInstanced(GL_TRIANGLES, 36, GL_UNSIGNED_INT, &elementIndex, numOfInstance);
	//
	cube_shader.disable();

	prototype->update();
	prototype->updateUniform(proj, view);
	prototype->drawInstances();
	

	cubic.enable();

		GLint Pu = cubic.getUniformLocation( "P" );
		GLint Vu = cubic.getUniformLocation( "V" );
		GLint Mu = cubic.getUniformLocation( "M" );
	    GLint C = cubic.getUniformLocation( "colour" );
		glUniformMatrix4fv( Pu, 1, GL_FALSE, value_ptr( proj ) );
		glUniformMatrix4fv( Vu, 1, GL_FALSE, value_ptr( view ) );
		glUniformMatrix4fv( Mu, 1, GL_FALSE, value_ptr( world ) );
		glUniform3f( C, 1.0f, 1.0f, 1.0f );

	cubic.disable();
	
	protocube->drawInstances();
	
	/*
	sp_shader.enable();

		glUniformMatrix4fv( P_sp, 1, GL_FALSE, value_ptr( proj ) );
		glUniformMatrix4fv( V_sp, 1, GL_FALSE, value_ptr( view ) );
		glUniformMatrix4fv( M_sp, 1, GL_FALSE, value_ptr( glm::mat4(1) ) );
		glUniform4f( col_sp, 1, 1, 1, 1 );

		glBindVertexArray(spVAO);
		glDrawElements(GL_TRIANGLES, 60, GL_UNSIGNED_INT, spIndex);

	sp_shader.disable();
	*/



	// Restore defaults
	glBindVertexArray( 0 );

	CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
/*
 * Called once, after program is signaled to terminate.
 */
void A1::cleanup()
{}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles cursor entering the window area events.
 */
bool A1::cursorEnterWindowEvent (
		int entered
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse cursor movement events.
 */
bool A1::mouseMoveEvent(double xPos, double yPos) 
{
	bool eventHandled(false);

	if (!ImGui::IsMouseHoveringAnyWindow()) {
		// Put some code here to handle rotations.  Probably need to
		// check whether we're *dragging*, not just moving the mouse.
		// Probably need some instance variables to track the current
		// rotation amount, and maybe the previous X position (so 
		// that you can rotate relative to the *change* in X.
		if(clicked){
			radian = (xPos - prevX)/1024.0f;

			prevX = xPos;

			updateMaze = true;
			interrupted = false;
		}
		eventHandled = true;
	}

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse button events.
 */
bool A1::mouseButtonInputEvent(int button, int actions, int mods) {
	bool eventHandled(false);

	if (!ImGui::IsMouseHoveringAnyWindow()) {
		// The user clicked in the window.  If it's the left
		// mouse button, initiate a rotation.
		if(button == GLFW_MOUSE_BUTTON_LEFT && actions == GLFW_PRESS){
			clicked = true;
			interrupted = false;
		} else if(button == GLFW_MOUSE_BUTTON_LEFT && actions == GLFW_RELEASE){
			clicked = false;
			released = true;
		}
		eventHandled = true;
	}

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse scroll wheel events.
 */
bool A1::mouseScrollEvent(double xOffSet, double yOffSet) {
	bool eventHandled(false);

	interrupted = true;

	// Zoom in or out.

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles window resize events.
 */
bool A1::windowResizeEvent(int width, int height) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles key input events.
 */
bool A1::keyInputEvent(int key, int action, int mods) {
	bool eventHandled(false);

	interrupted = true;

	// Fill in with event handling code...
	if( action == GLFW_PRESS ) {
		// Respond to some key events.
		if ( key == GLFW_KEY_SPACE ){
			heightModifier += 1;
		}

		if ( key == GLFW_KEY_BACKSPACE ){
			heightModifier -= 1;
		}
	}


	return eventHandled;
}
