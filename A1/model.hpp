//things sharing the same model really have a lot in common
//why not make a class for it?

#pragma once

#include <glm/glm.hpp>

#include "cs488-framework/CS488Window.hpp"
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/ShaderProgram.hpp"

#include <vector>

/*
 * Handles meshes, shaders and instancing to minimize draw calls
 * Should lead to better abstractions
 */

class Instance;


//TODO: implement generic version with some basic inputs for update and configureShader
//TODO: implement Projection and View uniforms as standard
//Enforcement of rules and limitations lead to better practices!

//TODO: texture
class Model {
    public:
    //By convention, we will be taking vertices that are 4 floats long
    Model(float* mesh, int* indice, int verticeCount, int polyCount, ShaderProgram& shader);
    int getId() const;
    static void generateId();
    //set up your vertex attribute input and layout here
    virtual void configureShader() = 0;
    //to prevent tight coupling, update is left to the implementation to allow for better integration with the shader
    virtual void update() = 0;
    //batched draw, does not prepare new vbo data
    virtual void drawInstances();
    virtual int registerInstance(Instance& instance);
    virtual ~Model();

    GLuint VAO;
    protected:
    ShaderProgram& shader;
    std::vector<Instance*> instances;

    int verticeCount;
    int polyCount;
    int* indice;
    //keep track of the number of instances
    int instanceCount;
    GLuint modelVBO;
    GLuint instanceVBO;

    private:
    int id;


};

