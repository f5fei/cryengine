// Winter 2020

#pragma once

#include <glm/glm.hpp>

#include "cs488-framework/CS488Window.hpp"
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/ShaderProgram.hpp"

#include "maze.hpp"

#include "sphere.hpp"
#include "avatar.hpp"
#include "cube.hpp"

class A1 : public CS488Window {
public:
	A1();
	virtual ~A1();

protected:
	virtual void init() override;
	virtual void appLogic() override;
	virtual void guiLogic() override;
	virtual void draw() override;
	virtual void cleanup() override;

	virtual bool cursorEnterWindowEvent(int entered) override;
	virtual bool mouseMoveEvent(double xPos, double yPos) override;
	virtual bool mouseButtonInputEvent(int button, int actions, int mods) override;
	virtual bool mouseScrollEvent(double xOffSet, double yOffSet) override;
	virtual bool windowResizeEvent(int width, int height) override;
	virtual bool keyInputEvent(int key, int action, int mods) override;

private:
	void initGrid();
	void compileCubeShader();
	void cubeShaderConfig();
	void generateHeightMap(Maze& m);
	void printMap();
	void computeCubeInstances();
	void initAvatar();
	void configureAvatarShader();


	// Fields related to the shader and uniforms.
	ShaderProgram m_shader;
	GLint P_uni; // Uniform location for Projection matrix.
	GLint V_uni; // Uniform location for View matrix.
	GLint M_uni; // Uniform location for Model matrix.
	GLint col_uni;   // Uniform location for cube colour.

	// Fields related to grid geometry.
	GLuint m_grid_vao; // Vertex Array Object for grid
	GLuint m_grid_vbo; // Vertex Buffer Object for grid

	ShaderProgram cube_shader;
	GLint P_unic;
	GLint V_unic;
	GLint M_unic;
	GLint col_unic;
	GLint H_unic;

	GLuint m_cube_vao; // Vertex Array Object for cube rendering

	GLuint m_model_vbo; // vertex buffer object for the model of the cube
	GLuint m_instance_vbo; //all the instance data will be here instead

	//Note: These dont seem to work, not sure why
	ShaderProgram airbender;
	sphere* prototype;
	avatar* player;

	ShaderProgram cubic;
	cube* protocube;
	Instance* ccc;


	//
	ShaderProgram sp_shader;
	GLint P_sp, V_sp, M_sp, col_sp;
	GLuint spVAO;
	GLuint modelVBO;
	int* spIndex;

	//keeps the render index
	float cube_mesh[8][3] = {
		{0,0,0},
		{1,0,0},
		{0,1,0},
		{1,1,0},
		{0,0,1},
		{1,0,1},
		{0,1,1},
		{1,1,1},
	};
	int elementIndex[36] = {
		6,7,3, 6,3,2, 2,3,1, 1,0,2, 7,5,3, 3,5,1, 2,4,6, 2,0,4, 0,1,4, 1,5,4, 6,4,7, 7,4,5
	};



	// Matrices controlling the camera and projection.
	glm::mat4 proj;
	glm::mat4 view;

	glm::mat4 world;

	float* heightMap;
	float* positions;
	float heightModifier;

	bool updateMaze;

	bool clicked;
	bool released;
	bool interrupted;
	float prevX;

	float radian;

	float* instancePayload;

	int numOfInstance;

	float colour[3];
	int current_col;
};
