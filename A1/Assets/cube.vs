#version 330


uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
in vec4 vertex;

void main(){
	
	mat4 heightMap = mat4(1.0, 0.0, 0.0, 0.0,
							0.0, 100, 0.0, 0.0,
							0.0, 0.0, 1.0, 0.0,
                            0.0, 0.0, 0.0, 1.0);

	gl_Position = P * V * M * vec4(heightMap * vertex);
}