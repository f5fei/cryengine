#version 330

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

in vec4 vx;

void main(){
    gl_Position = P * V * M * vx;
}