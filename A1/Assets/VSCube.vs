#version 330


uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform float H;
in vec3 vertex;
in vec3 pos;
in float height;

void main(){
	
	mat3 heightMap = mat3(1.0, 0.0, 0.0,
							0.0, height + H, 0.0,
							0.0, 0.0, 1.0);

	gl_Position = P * V * M * vec4(heightMap * vertex + pos, 1.0);
}