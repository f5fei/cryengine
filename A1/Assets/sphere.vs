#version 330

uniform mat4 P;
uniform mat4 V;

in vec4 vx;
in vec4 colour;
in mat4 M;
out vec4 color;

void main(){
    gl_Position = P * V * M * vx;
    color = colour;
}