#pragma once
#include "model.hpp"

class sphere : public Model {
    public:
    sphere(float* mesh, int* indice, int verticeCount, int polyCount, ShaderProgram& shader);
    static sphere* tessellateSpheres(int factor, ShaderProgram& shader);
    virtual void configureShader() override;
    virtual void update() override;
    void updateUniform(glm::mat4 proj, glm::mat4 view);
    virtual ~sphere();

    private:
    GLint P;
    GLint V;
};

void starterIcosahedron(float* vertices, int* polygons, int stride);

