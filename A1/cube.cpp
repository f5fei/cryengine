#include "cube.hpp"
#include <glm/glm.hpp>

#include "cs488-framework/CS488Window.hpp"
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/ShaderProgram.hpp"

#include <iostream>
using namespace std;

cube* getNewCube(ShaderProgram& shader){
    float* vs = new float[32];
    for(int i=0; i<32;i++){
        vs[i] = 0;
    }

    vs[4] = 1;
    vs[9] = 1;
    vs[12] = 1;
    vs[13] = 1;
    vs[18] = 1;
    vs[20] = 1;
    vs[22] = 1;
    vs[25] = 1;
    vs[26] = 10;
    vs[28] = 1;
    vs[29] = 1;
    vs[30] = 1;
	int elementIndex[36] = {
		6,7,3, 6,3,2, 2,3,1, 1,0,2, 7,5,3, 3,5,1, 2,4,6, 2,0,4, 0,1,4, 1,5,4, 6,4,7, 7,4,5
	};

    int* ind = new int[36];
    for(int i=0; i<36; i++){
        ind[i] = elementIndex[i];
    }

    return new cube(vs, ind, 8, 12, shader);

}

cube::cube(float* mesh, int* indice, int verticeCount, int polyCount, ShaderProgram& shader): Model(mesh, indice, verticeCount, polyCount, shader){
}


void cube::configureShader(){
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, modelVBO);
    GLint vAtr = shader.getAttribLocation("vertex");
    glEnableVertexAttribArray( vAtr );
    glVertexAttribPointer(vAtr, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), nullptr);
    glBindVertexArray(0);
    glBindBuffer( GL_ARRAY_BUFFER, 0);

}

void cube::update(){




}
