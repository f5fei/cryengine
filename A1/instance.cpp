#pragma once

#include "instance.hpp"
#include "model.hpp"

Instance::Instance(Model& m, glm::mat4& transformation): model(m), transformation(transformation){
    id = m.registerInstance(*this);
}

Instance::~Instance(){

}