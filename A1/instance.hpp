#pragma once

#include <glm/glm.hpp>

class Model;

class Instance {
    friend class Model;
    public:
    Instance(Model& m, glm::mat4& transformation);
    //to be implemented:
    //how to make model transformations

    //translation

    //reflection
    
    //rotation


    virtual ~Instance();

    protected:
    //use this to generate the correct vertex rotations
    glm::mat4 transformation;
    Model& model;


    private:
    int id;

};