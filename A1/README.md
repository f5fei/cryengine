CS488 A1
Build and Compile:
cd to the cs488 folder
premake4 gmake
make
cd to the A1 folder
premake4 gmake
make
./A1


Assumptions:
Rotation is based on last vector
Persistence means free spinning motion until keyboard input
Cubes and other meshes should be instanced and rendered with drawElements


Completed Features:
you can use space and backspace to change the wall height, this is done with a uniform height modifier to leverage the parallelism of the vertex shaders
space will make the wall go higher, backspace will shrink the wall
the values are unbound, but it is easy to recover: one space cancels out a backspace and vice verse

all the geometries rendered make use of instanced rendering and element indices. This should lead to better performance and less draw calls.

Rotation is achieved if you click and drag any point inside the window. if you let go, it will keep spinning until a keyboard event or a mouse event.


Incomplete/Buggy Features:
I've made two attempts to draw the sphere player character using isocohedron, for reasons unkown to me it does not work. 

I've suspected that perhaps my Model/Instance abstraction does not keep some data consistently, but instances are correctly registered and vertice data is well preserved when I actually print debug statements.

I've suspected that its because I apply the uniform in a separated shader enable disable block than my actual draw call, but trying to move uniforms out of my working codeblock into its own enable disable block didnt prevent them from being rendered, so this is not likely the case

For sanity check, I attempted to ditch my abstractions as well as instance rendering, but it does not appear.

It's not due to the vertice data (at least not sole due to it) as my cubic shader also fails to render something.

My current suspision was on the model matrix causing an negative effect due to an incosnsitent coordinate system, but I was not able to confirm that.

Had I known how prone to bug my code is, I most likely should not/would not have implemented the abstraction.