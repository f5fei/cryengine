
#pragma once
#include "model.hpp"

class cube : public Model{
    public:
    cube(float* mesh, int* indice, int verticeCount, int polyCount, ShaderProgram& shader);
    void configureShader();
    void update();
    private:
    GLint P, V, M;
};

cube* getNewCube(ShaderProgram& shader);