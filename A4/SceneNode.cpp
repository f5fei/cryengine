// Winter 2020

#include "SceneNode.hpp"

#include "cs488-framework/MathUtils.hpp"

#include <iostream>
#include <sstream>
using namespace std;

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>

using namespace glm;


// Static class variable
unsigned int SceneNode::nodeInstanceCount = 0;


//---------------------------------------------------------------------------------------
SceneNode::SceneNode(const std::string& name)
  : m_name(name),
	m_nodeType(NodeType::SceneNode),
	trans(mat4(1)),
	invtrans(mat4(1)),
	rot(mat4(1)),
	invRot(mat4(1)),
	scl(mat4(1)),
	invScl(mat4(1)),
	tl(mat4(1)),
	invTl(mat4(1)),
	transformCache(mat4(1)),
	m_nodeId(nodeInstanceCount++),
	parent(nullptr)
{

}

//---------------------------------------------------------------------------------------
// Deep copy
SceneNode::SceneNode(const SceneNode & other)
	: m_nodeType(other.m_nodeType),
	  m_name(other.m_name),
	  trans(other.trans),
	  invtrans(other.invtrans),
	  parent(other.parent)
{
	for(SceneNode * child : other.children) {
		this->children.push_front(new SceneNode(*child));
	}
}

//---------------------------------------------------------------------------------------
SceneNode::~SceneNode() {
	for(SceneNode * child : children) {
		delete child;
	}
}

//---------------------------------------------------------------------------------------
void SceneNode::set_transform(const glm::mat4& m) {
	trans = m;
	invtrans = glm::inverse(m);
}

//---------------------------------------------------------------------------------------
const glm::mat4& SceneNode::get_transform(){
	
	trans = tl * scl * rot;

	return trans;
}

//---------------------------------------------------------------------------------------
const glm::mat4& SceneNode::get_inverse(){

	invtrans = invRot * invScl * invTl;

	return invtrans;
}

//---------------------------------------------------------------------------------------
void SceneNode::add_child(SceneNode* child) {
	children.push_back(child);
	child->parent = this;
}

//---------------------------------------------------------------------------------------
void SceneNode::remove_child(SceneNode* child) {
	children.remove(child);
	child->parent = nullptr;
}


//--------------------------------------------------------------------------------------
glm::vec3 SceneNode::getAxis(int axi){
	float b[4] = {0, 0, 0, 0};
	b[axi] = 1;
	vec4 basis = make_vec4(b);
	basis = rot * basis;
	basis = tl * basis;
	return vec3(basis);
}


//---------------------------------------------------------------------------------------
void SceneNode::rotate(char axis, float angle) {
	vec3 rot_axis = getAxis(axis - 'X');
	mat4 rot_matrix = glm::rotate(degreesToRadians(angle), rot_axis);
	mat4 rev = glm::rotate(degreesToRadians(angle * - 1), rot_axis);
	rot = rot_matrix * rot;
	invRot = invRot * rev;
}

//---------------------------------------------------------------------------------------
void SceneNode::scale(const glm::vec3 & amount) {
	const float* sc = value_ptr(amount);
	float invs[3];
	for(int i=0; i<3; i++){
		invs[i] = 1.0f / sc[i];
	}
	vec3 iamnt = make_vec3(invs);
	mat4 pScale = glm::scale(amount);
	mat4 iScale = glm::scale(iamnt);

	scl = pScale * scl;
	invScl = invScl * iScale;

}

//---------------------------------------------------------------------------------------
void SceneNode::translate(const glm::vec3& amount) {
	mat4 tr = glm::translate(amount);
	mat4 itr = glm::translate(amount * -1.0f);

	tl = tr * tl;
	invTl = invTl * itr;
}



//---------------------------------------------------------------------------------------
void SceneNode::propagateTransformation(){	
	mat4 trans = get_transform();
	mat4 comp = mat4(1);

	if(parent)
		comp = parent->transformCache * comp;
	
	//as comp is globally applied before the basis of the transformations, it comes first
	transformCache = trans * comp;

	for(SceneNode* node : children){
		node->propagateTransformation();
	}

}

//---------------------------------------------------------------------------------------
int SceneNode::totalSceneNodes() const {
	return nodeInstanceCount;
}

//---------------------------------------------------------------------------------------
std::ostream & operator << (std::ostream & os, const SceneNode & node) {

	//os << "SceneNode:[NodeType: ___, name: ____, id: ____, isSelected: ____, transform: ____"
	switch (node.m_nodeType) {
		case NodeType::SceneNode:
			os << "SceneNode";
			break;
		case NodeType::GeometryNode:
			os << "GeometryNode";
			break;
		case NodeType::JointNode:
			os << "JointNode";
			break;
	}
	os << ":[";

	os << "name:" << node.m_name << ", ";
	os << "id:" << node.m_nodeId;

	os << "]\n";
	return os;
}
