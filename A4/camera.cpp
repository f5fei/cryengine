#include "camera.hpp"
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace glm;

basis produceBasis(glm::vec3 lookFrom, glm::vec3 view, glm::vec3 up, double yfov, int w, int h){
	basis b;
	b.lookFrom = lookFrom;
	vec3 lookAt = view - lookFrom;
	lookAt = normalize(lookAt);
	vec3 h_transform = normalize(cross(lookAt, up));
	vec3 v_transform = normalize(cross(lookAt, h_transform));
	
	double xfov = yfov * w / h;

	float vi = tan(yfov/2*3.141592653/180);
	float hi = tan(xfov/2*3.141592653/180);

	b.lookAt = lookAt;
	
	b.dx = h_transform * hi * (2.0f / w);
	b.dy = v_transform * vi * (2.0f / h);

	b.frame = lookAt - vi * v_transform - hi * h_transform;

	return b;
}
