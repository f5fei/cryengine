#include "intersect.hpp"
#include "Entity.hpp"
#include "Ray.hpp"

ltx::intersect::intersect(float t, Entity& obj):Intersection(t), entity(obj){
}


int ltx::intersect::getEntityId(){
	entity.getId();
}

const glm::vec3& ltx::intersect::getNormal(){
	return disabled;
}

const glm::vec3& ltx::intersect::getPosition(){
	return disabled;
}

const glm::vec3& ltx::intersect::getDiffusion(){
	return disabled;
}

const glm::vec3& ltx::intersect::getSpecular(){
	return disabled;
}

const glm::vec3& ltx::intersect::getTransparency(){
	return disabled;
}

const glm::vec3& ltx::intersect::getDiffraction(){
	return disabled;
}

const glm::vec3& ltx::intersect::getLight(){
	return disabled;
}

void ltx::intersect::setColouring(ltx::Ray& ray){

}

const std::vector<ltx::Ray> ltx::intersect::getColouring(){
	return std::vector<Ray>();
}

ltx::intersect::~intersect(){}
