#include "Renderer.hpp"
#include "Intersection.hpp"
#include "Scene.hpp"
#include "Ray.hpp"
#include "LightSource.hpp"
#include <cmath>
#include "vectorOp.hpp"
#include "debugging.hpp"

using namespace glm;

ltx::Renderer::Renderer(){	
}

//TODO: eventually
void ltx::Renderer::rayCast(ltx::Ray& light, ltx::Scene& scene){
	Intersection* result = scene.intersect(light);
	
	if(!result)
		return;

	//we are offloading the work to the ray tracer, as there are (probably) less light traced than there are cast
	result->setColouring(light);
	
	vec3 norm = result->getNormal();
	//do something about the diffusive light source

	//do something about specular reflection
	vec3 specl = lllm::hadamardProduct(light.getIntensity(), result->getSpecular()) * specularCost;
	if(length(specl) > threshold){	
		vec3 bounce = reflect(light.getDirection(), norm);
		ltx::Ray reflect = Ray(result->getPosition(), bounce, specl);
		rayCast(reflect, scene);
	}
	//do something about transparency
	//to be continued


	//do something about diffraction 
	//later


	//cleanup
	delete result;
}

glm::vec3 ltx::Renderer::rayTrace(ltx::Ray& probe, ltx::Scene& scene){
	Intersection* result = scene.intersect(probe);
	vec3 resultantLight = vec3(0);
	if(!result){
		//do something interesting later
		return vec3(0, 0, 0);
	}
	
	vec3 dirLight = result->getLight();
	if(length(dirLight) > ambientLightThreshold){
		return dirLight;
	}

	vec3 position = result->getPosition();
	vec3 diffuse = result->getDiffusion();
	vec3 specular = result->getSpecular();
	vec3 normal = normalize(result->getNormal());

	vec3 incident = normalize(reflect(probe.getDirection(), normal));
	//TODO: Also do this for transparency and diffraction


	const std::vector<LightSource*>& lights = scene.getLightSources();


	for(LightSource* light : lights){
		//TODO: do light source exclusion
		//why not now: efficient implementation require light source ids
		

		//only do it anyways if it not blocked
		
		//diffuse light
		vec3 dir = normalize(light->getPosition() - result->getPosition());
		//light doesn't matter, we are just interested in the ray tracing
		Ray probeRay = Ray(position, dir, dir);
		//TODO: optimize intersect for transparency & object search


		float diff = dot(normal, dir);

		float inc = dot(incident, dir);

		//TODO: handle transparency
		
		Intersection* block = scene.intersect(probeRay, 0.01);
		if(!block || block->getEntityId() == light->getId() ){//no block present
			resultantLight = resultantLight + lllm::hadamardProduct(diffuse, light->getLightCast(position)) * diff * diffuseCost;

			resultantLight = resultantLight + lllm::hadamardProduct(specular, light->getLightCast(position)) * inc * specularCost;
		}

		//delete block;
		delete block;

		//specular value
		//we assume, without another dumb ray casting, that this is sufficiently close to the source
		/*resultantLight = resultantLight + lllm::hadamardProduct(specular, light->getLightCast(position)) * inc * specularCost;*/
	}

	//disables ambient light
	resultantLight = resultantLight + lllm::hadamardProduct(diffuse, ambientLight);

	//specular ray tracing
	vec3 specularVolume = lllm::hadamardProduct(specular, probe.getIntensity()) * specularCost;
	if(length(specularVolume) >= threshold){
		Ray specProb = Ray(position, incident, specularVolume);
		vec3 color = rayTrace(specProb, scene);
		resultantLight = resultantLight + color;
	}
	
	delete result;
	return resultantLight;
}
