#include "inScene.hpp"
#include "Ray.hpp"
#include "LightSource.hpp"
#include "Intersection.hpp"
#include "pointLight.hpp"
#include "GeometryEntity.hpp"
#include <vector>
#include "SceneNode.hpp"
#include "debugging.hpp"

inScene::inScene(SceneNode& root):root(root){}

ltx::Intersection* inScene::intersect(const ltx::Ray& ray, float t, float stepSize, float backOff){
	//we are not implementing iterative steps, just naive compare
	
	ltx::Intersection* result = nullptr;

	//used for baking hierarchical transformation
	root.propagateTransformation();

	std::vector<GeometryEntity*>& entities = GeometryEntity::getEntities();
	std::vector<ltx::LightSource*>& lights = pointLight::getLightSources();

	for(ltx::LightSource* light : lights){
		ltx::Intersection* hit = light->intersect(ray);
		if(!result){
			result = hit;
		} else if( hit && result->getRayDist() > hit->getRayDist() && hit->getRayDist() > t){
			delete result;
			result = hit;
		}
	}

	for(GeometryEntity* obj : entities){
		ltx::Intersection* hit = obj->intersect(ray);
		if(!result){
			result = hit;
		} else if( hit && result->getRayDist() > hit->getRayDist() && hit->getRayDist() > t){
			delete result;
			result = hit;
		}
	}
	
	return result;
}

std::vector<ltx::LightSource*>& inScene::getLightSources(){
	return pointLight::getLightSources();
}

inScene::~inScene(){}
