#pragma once

#include "Scene.hpp"

class SceneNode;

class inScene : public ltx::Scene{

	SceneNode& root;
	
	public:
	inScene(SceneNode& root);

	virtual ltx::Intersection* intersect(const ltx::Ray& ray, float t=0, float stepSize = 1, float backOff = 1.25);
	virtual std::vector<ltx::LightSource*>& getLightSources();
	virtual ~inScene();
};
