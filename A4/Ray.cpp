#include "Ray.hpp"

using namespace std;

ltx::Ray::Ray(const glm::vec3& source, const glm::vec3& direction, const glm::vec3& intensity):source(source), direction(direction), intensity(intensity){
}

const glm::vec3& ltx::Ray::getSource() const{
	return source;
}

const glm::vec3& ltx::Ray::getDirection() const{
	return direction;
}

const glm::vec3& ltx::Ray::getIntensity() const{
	return intensity;
}
