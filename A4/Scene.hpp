#pragma once

#include <vector>

namespace ltx{
class Intersection;
class Ray;
class LightSource;
class Scene{
	public:
	Scene();
	//t = > start after this parameter, allows for skipping of previously interacted objects (useful for transpanrency
	//stepSize = > iterative intersection alg params, 0 => global search
	//backOff => exponential backoff
	virtual Intersection* intersect(const Ray& ray, float t = 0, float stepsize = 1, float backOff = 1.25) = 0;
	virtual const std::vector<LightSource*>& getLightSources() = 0;
	virtual ~Scene();
};
}
