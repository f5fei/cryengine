#pragma once
#include "intersect.hpp"
#include "Entity.hpp"

class geoIntersect : public ltx::intersect {	
	glm::vec3 normal;
	glm::vec3 position;
	glm::vec3 specular;
	glm::vec3 diffuse;
	
	public:
	geoIntersect(float t, ltx::Entity& obj, const glm::vec3& normal, const glm::vec3& position, const glm::vec3& specular, const glm::vec3& diffuse);
	virtual const glm::vec3& getNormal();
	virtual const glm::vec3& getPosition();
	virtual const glm::vec3& getSpecular();
	virtual const glm::vec3& getDiffusion();
	~geoIntersect();
};
