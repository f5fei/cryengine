#include "geoIntersect.hpp"

geoIntersect::geoIntersect(float t, ltx::Entity& obj, const glm::vec3& normal, const glm::vec3& position, const glm::vec3& specular, const glm::vec3& diffuse):intersect(t, obj), normal(normal), position(position), specular(specular), diffuse(diffuse){}

const glm::vec3& geoIntersect::getNormal(){
	return normal;
}

const glm::vec3& geoIntersect::getPosition(){
	return position;
}

const glm::vec3& geoIntersect::getSpecular(){
	return specular;
}

const glm::vec3& geoIntersect::getDiffusion(){
	return diffuse;
}

geoIntersect::~geoIntersect(){

}
