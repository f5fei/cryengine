#include "GeometryEntity.hpp"
#include "geoIntersect.hpp"
#include "GeometryNode.hpp"
#include "debugging.hpp"

std::vector<GeometryEntity*> GeometryEntity::entities = std::vector<GeometryEntity*>();

GeometryEntity::GeometryEntity(int id, GeometryNode& node): ltx::Entity(id), source(node){
	entities.push_back(this);
}

ltx::Intersection* GeometryEntity::intersect(const ltx::Ray& ray){
	return source.intersect(ray, *this);
}

const glm::vec3& GeometryEntity::getSpecular(){
	return source.m_material->getSpecular();
}

const glm::vec3& GeometryEntity::getDiffuse(){
	return source.m_material->getDiffuse();
}

std::vector<GeometryEntity*>& GeometryEntity::getEntities(){
	return entities;
}
