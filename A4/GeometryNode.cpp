// Winter 2020

#include "GeometryNode.hpp"
#include "geoIntersect.hpp"
#include "Primitive.hpp"
#include "GeometryEntity.hpp"
#include "Ray.hpp"
#include "debugging.hpp"

using namespace glm;

//---------------------------------------------------------------------------------------
GeometryNode::GeometryNode(
	const std::string & name, Primitive *prim, Material *mat )
	: SceneNode( name )
	, m_material( mat )
	, m_primitive( prim )
{
	m_nodeType = NodeType::GeometryNode;
	GeometryEntity* ge = new GeometryEntity(m_nodeId, *this);
}

void GeometryNode::setMaterial( Material *mat )
{
	// Obviously, there's a potential memory leak here.  A good solution
	// would be to use some kind of reference counting, as in the 
	// C++ shared_ptr.  But I'm going to punt on that problem here.
	// Why?  Two reasons:
	// (a) In practice we expect the scene to be constructed exactly
	//     once.  There's no reason to believe that materials will be
	//     repeatedly overwritten in a GeometryNode.
	// (b) A ray tracer is a program in which you compute once, and 
	//     throw away all your data.  A memory leak won't build up and
	//     crash the program.

	m_material = mat;
}

ltx::Intersection* GeometryNode::intersect(const ltx::Ray& ray, GeometryEntity& entity){
	pointHit* hit = m_primitive->intersect(ray, this);
	
	if(!hit){
		return nullptr;
	}
	
	vec3 norm = hit->normal;
	vec3 specular = m_material->getSpecular();
	vec3 diffuse = m_material->getDiffuse();

	float t = hit->t;
	vec3 position = ray.getSource() + ray.getDirection() * t;

	delete hit;
	return new geoIntersect(t, entity, position, norm, specular, diffuse);
}
