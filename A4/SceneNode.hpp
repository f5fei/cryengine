// Winter 2020

#pragma once

#include "Material.hpp"

#include <glm/glm.hpp>

#include <list>
#include <string>
#include <iostream>

enum class NodeType {
	SceneNode,
	GeometryNode,
	JointNode
};

class SceneNode {
public:
    SceneNode(const std::string & name);

	SceneNode(const SceneNode & other);

    virtual ~SceneNode();
    
	int totalSceneNodes() const;
    
    const glm::mat4& get_transform();
    const glm::mat4& get_inverse();
    
    void set_transform(const glm::mat4& m);
    
    void add_child(SceneNode* child);
    
    void remove_child(SceneNode* child);

	glm::vec3 getAxis(int axi);

	//-- Transformations:
    void rotate(char axis, float angle);
    void scale(const glm::vec3& amount);
    void translate(const glm::vec3& amount);

	//"prebaked" hierarchical transformations
	void propagateTransformation();


	friend std::ostream & operator << (std::ostream & os, const SceneNode & node);
    // Transformations
    glm::mat4 trans;
    glm::mat4 invtrans;

	//cached or baked transformations
	glm::mat4 transformCache;

	//rotation
	glm::mat4 rot;
	glm::mat4 invRot;

	//Note: This will work for axis but definitely not normals
	glm::mat4 scl;
	glm::mat4 invScl;

	// Translations
	glm::mat4 tl;
	glm::mat4 invTl;

    std::list<SceneNode*> children;

	NodeType m_nodeType;
	std::string m_name;
	unsigned int m_nodeId;

	SceneNode* parent;

private:
	// The number of SceneNode instances.
	static unsigned int nodeInstanceCount;
};
