#include "Entity.hpp"
#include "Ray.hpp"

using namespace glm;


ltx::Entity::Entity(int id):entityId(id){
}

ltx::Entity::Entity(int id, const vec3& position):entityId(id), Position(position){}

int ltx::Entity::getId() const{
	return entityId;
}

const glm::vec3& ltx::Entity::getPosition() const{
	return Position;
}

ltx::Intersection* ltx::Entity::intersect(const ltx::Ray& ray){
	return nullptr;
}

ltx::Entity::~Entity(){}
