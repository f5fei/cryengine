#pragma once

#include <glm/glm.hpp>
#include <vector>
#include "Entity.hpp"

namespace ltx{

class Intersection;

class LightSource: public Entity{
	glm::vec3 Radiance;
	public:
	LightSource(int id, const glm::vec3& origin, const glm::vec3& color);
	virtual glm::vec3 getLightCast(const glm::vec3& position) const;
	virtual const std::vector<Ray> sampleCastRays(int mult) const;
	virtual Intersection* intersect(const Ray& ray);
	virtual ~LightSource();
};

}
