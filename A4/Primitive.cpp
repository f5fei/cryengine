// Winter 2020

#include "Primitive.hpp"
#include "Ray.hpp"
#include "GeometryNode.hpp"
#include "epsilon.hpp"
#include <glm/gtc/type_ptr.hpp>

using namespace glm;


pointHit* Primitive::intersect(const ltx::Ray& ray, GeometryNode* parent){
	return nullptr;
}

Primitive::~Primitive()
{
}

pointHit* Sphere::intersect(const ltx::Ray& ray, GeometryNode* parent){
	return nullptr;
}


Sphere::~Sphere()
{
}

pointHit* Cube::intersect(const ltx::Ray& ray, GeometryNode* parent){
	return nullptr;
}

Cube::~Cube()
{
}

NonhierSphere::NonhierSphere(const glm::vec3& pos, double radius): m_pos(pos), m_radius(radius){
}

pointHit* NonhierSphere::intersect(const ltx::Ray& ray, GeometryNode* parent){
	//as these are none hierarchical, the parent node literally doesn't matter
	vec3 diff = m_pos - ray.getSource();
	float dis = dot(diff, ray.getDirection());
	vec3 clos = ray.getSource() + ray.getDirection() * dis;
	vec3 closNorm = clos - m_pos;
	float closest = length(closNorm);
	float t = closest;
	if(lllf::equals(closest, m_radius)){
		//one point
		return new pointHit{normalize(closNorm), clos, t};
	} else if(closest < m_radius){
		//retrace
		float retrace = sqrt(m_radius * m_radius - closest * closest);
		t -= retrace;
		//assumes that the direction vector is normalized, i bloody hope it is
		vec3 firstTouch = clos - ray.getDirection() * retrace;
		vec3 norm = firstTouch - m_pos;
		return new pointHit{normalize(norm), firstTouch, t};
	}

	return nullptr;
}


NonhierSphere::~NonhierSphere()
{
}

NonhierBox::NonhierBox(const glm::vec3& pos, double size): m_pos(pos), m_size(size){}


pointHit* NonhierBox::intersect(const ltx::Ray& ray, GeometryNode* parent){

	vec3 point = ray.getSource();
	vec3 dir = ray.getDirection();
	vec3 diff = m_pos - point;
	const float* ps = value_ptr(diff);
	const float* ds = value_ptr(dir);

	float lowT = -1;
	vec3 lposn;
	vec3 lnorm;

	float t;
	vec3 pos;
	bool fit = false;
	t = 0 + ps[0];
	pos = point + t * dir;
	const float* cp = value_ptr(pos);
	
	if(lllf::greaterThan(cp[1], 0) && lllf::lessThan(cp[1], m_size)
		&& lllf::greaterThan(cp[2], 0) && lllf::lessThan(cp[2], m_size)){
		fit = true;
	}

	if(lowT == -1 && t > 0 && fit){
		lowT = t;
		lposn = pos;
		lnorm = vec3(-1, 0, 0);
	}
	
	t = m_size + ps[0];
	pos = point + t * dir;
	cp = value_ptr(pos);
	
	if(lllf::greaterThan(cp[1], 0) && lllf::lessThan(cp[1], m_size)
		&& lllf::greaterThan(cp[2], 0) && lllf::lessThan(cp[2], m_size)){
		fit = true;
	}

	if(lowT == -1 && t > 0 && fit){
		lowT = t;
		lposn = pos;
		lnorm = vec3(1, 0, 0);
	}

	t = 0 + ps[1];
	pos = point + t * dir;
	cp = value_ptr(pos);
	
	if(lllf::greaterThan(cp[0], 0) && lllf::lessThan(cp[0], m_size)
		&& lllf::greaterThan(cp[2], 0) && lllf::lessThan(cp[2], m_size)){
		fit = true;
	}

	if(lowT == -1 && t > 0 && fit){
		lowT = t;
		lposn = pos;
		lnorm = vec3(0, -1, 0);
	}

	t = m_size + ps[1];
	pos = point + t * dir;
	cp = value_ptr(pos);
	
	if(lllf::greaterThan(cp[0], 0) && lllf::lessThan(cp[0], m_size)
		&& lllf::greaterThan(cp[2], 0) && lllf::lessThan(cp[2], m_size)){
		fit = true;
	}

	if(lowT == -1 && t > 0 && fit){
		lowT = t;
		lposn = pos;
		lnorm = vec3(0, 1, 0);
	}

	t = 0 + ps[2];
	pos = point + t * dir;
	cp = value_ptr(pos);
	
	if(lllf::greaterThan(cp[0], 0) && lllf::lessThan(cp[0], m_size)
		&& lllf::greaterThan(cp[1], 0) && lllf::lessThan(cp[1], m_size)){
		fit = true;
	}

	if(lowT == -1 && t > 0 && fit){
		lowT = t;
		lposn = pos;
		lnorm = vec3(0, 0, -1);
	}

	t = m_size + ps[2];
	pos = point + t * dir;
	cp = value_ptr(pos);
	
	if(lllf::greaterThan(cp[0], 0) && lllf::lessThan(cp[0], m_size)
		&& lllf::greaterThan(cp[1], 0) && lllf::lessThan(cp[1], m_size)){
		fit = true;
	}

	if(lowT == -1 && t > 0 && fit){
		lowT = t;
		lposn = pos;
		lnorm = vec3(0, 0, 1);
	}

	return new pointHit{lnorm, lposn, lowT};

}


NonhierBox::~NonhierBox()
{
}
