#pragma once

#include <glm/glm.hpp>


//funnily enough, probe ray and light ray have exactly the same requirements and dimensionality

namespace ltx{
class Ray{
	glm::vec3 source;
	glm::vec3 direction;
	glm::vec3 intensity;
	public:
	Ray(const glm::vec3& source, const glm::vec3& direction, const glm::vec3& intensity);
	const glm::vec3& getSource() const;
	const glm::vec3& getDirection() const;
	const glm::vec3& getIntensity() const;
};
}
