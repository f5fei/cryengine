// Winter 2020

#pragma once

#include <glm/glm.hpp>


namespace ltx{
class Ray;
}

class GeometryNode;

//what to return for hits
struct pointHit{
	glm::vec3 normal;
	glm::vec3 params;
	float t;
};

class Primitive {
public:
	virtual pointHit* intersect(const ltx::Ray& ray, GeometryNode* parent);
 	virtual ~Primitive();
};

class Sphere : public Primitive {
public:
 	virtual pointHit* intersect(const ltx::Ray& ray, GeometryNode* parent);
	virtual ~Sphere();
};

class Cube : public Primitive {
public:
 	virtual pointHit* intersect(const ltx::Ray& ray, GeometryNode* parent);
	virtual ~Cube();
};

class NonhierSphere : public Primitive {
public:
 	NonhierSphere(const glm::vec3& pos, double radius);
	virtual pointHit* intersect(const ltx::Ray& ray, GeometryNode* parent);
 	virtual ~NonhierSphere();

private:
  glm::vec3 m_pos;
  double m_radius;
};

class NonhierBox : public Primitive {
public:
	NonhierBox(const glm::vec3& pos, double size);
	virtual pointHit* intersect(const ltx::Ray& ray, GeometryNode* parent);
	virtual ~NonhierBox();

private:
  glm::vec3 m_pos;
  double m_size;
};
