#pragma once

#include "Material.hpp"
#include <glm/glm.hpp>

class simpleMaterial : public Material{
	glm::vec3 diffuse;
	glm::vec3 specular;
	public:
	simpleMaterial(const glm::vec3& diffuse, const glm::vec3& specular);
	virtual const glm::vec3& getDiffuse();
	virtual const glm::vec3& getSpecular();
	virtual ~simpleMaterial();
};
