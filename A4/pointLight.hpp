#pragma once

#include "LightSource.hpp"
#include <vector>

struct Light;

class pointLight : public ltx::LightSource{
	Light& source;
	static int lightId;
	static std::vector<LightSource*> lights;
	public:
	pointLight(Light& source);
	virtual glm::vec3 getLightCast(const glm::vec3& position);
	static std::vector<LightSource*>& getLightSources();
	static void registerLight(Light& source);
	virtual ~pointLight();
};
