// Winter 2020

#include <glm/ext.hpp>

#include "A4.hpp"

#include "Renderer.hpp"

#include "Ray.hpp"

#include "camera.hpp"

#include "inScene.hpp"

#include "debugging.hpp"

void A4_Render(
		// What to render  
		SceneNode * root,

		// Image to write to, set to a given width and height  
		Image & image,

		// Viewing parameters  
		const glm::vec3 & eye,
		const glm::vec3 & view,
		const glm::vec3 & up,
		double fovy,

		// Lighting parameters  
		const glm::vec3 & ambient,
		const std::list<Light *> & lights
) {

  // Fill in raytracing code here...  

  std::cout << "Calling A4_Render(\n" <<
		  "\t" << *root <<
          "\t" << "Image(width:" << image.width() << ", height:" << image.height() << ")\n"
          "\t" << "eye:  " << glm::to_string(eye) << std::endl <<
		  "\t" << "view: " << glm::to_string(view) << std::endl <<
		  "\t" << "up:   " << glm::to_string(up) << std::endl <<
		  "\t" << "fovy: " << fovy << std::endl <<
          "\t" << "ambient: " << glm::to_string(ambient) << std::endl <<
		  "\t" << "lights{" << std::endl;

	for(const Light * light : lights) {
		std::cout << "\t\t" <<  *light << std::endl;
	}
	std::cout << "\t}" << std::endl;
	std:: cout <<")" << std::endl;

	size_t h = image.height();
	size_t w = image.width();

	basis frame = produceBasis(eye, view, up, fovy, w, h);

	ltx::Renderer render{};
	render.threshold = 0.1;
	render.specularCost = 0.5;
	render.diffuseCost = 0.5;
	render.incidenceThreshold = 0.01;
	render.ambientLightThreshold = 0.1;
	render.ambientLight = ambient;
	inScene scene(*root);


	glm::vec3 intensity = glm::vec3(1);

	for (uint y = 0; y < h; ++y) {
		for (uint x = 0; x < w; ++x) {
			
			glm::vec3 dir = frame.frame + frame.dx * x + frame.dy * y;
			
			dir = normalize(dir);

			ltx::Ray probe{frame.lookFrom, dir, intensity};

			glm::vec3 pix = render.rayTrace(probe, scene);	

			const float* pv = glm::value_ptr(pix);

			// Red: 
			image(x, y, 0) = (double)pv[0];
			// Green: 
			image(x, y, 1) = (double)pv[1];
			// Blue: 
			image(x, y, 2) = (double)pv[2];
		}
	}

	




}
