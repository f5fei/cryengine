#pragma once

#include <glm/glm.hpp>
#include <vector>

namespace ltx{

class Intersection;

class Ray;

//Geometric representation of all entities in a scene, be it light source or not
class Entity{
	int entityId;
	protected:
	glm::vec3 Position;

	public:
	Entity(int id);
	Entity(int id, const glm::vec3& position);
	int getId() const;
	//ray intersection
	virtual Intersection* intersect(const Ray& ray);
	//get lighting properties
	virtual const glm::vec3& getPosition() const;
	~Entity();
};
}
