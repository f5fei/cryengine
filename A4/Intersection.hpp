#pragma once

#include <glm/glm.hpp>
#include <vector>

namespace ltx{

class Ray;
class Intersection{
	protected:
	float t;
	public:
	Intersection(float t);
	virtual float getRayDist();
	virtual int getEntityId() = 0;
	virtual const glm::vec3& getNormal() = 0;
	virtual const glm::vec3& getPosition() = 0;
	virtual const glm::vec3& getDiffusion() = 0;
	virtual const glm::vec3& getSpecular() = 0;
	virtual const glm::vec3& getTransparency() = 0;
	virtual const glm::vec3& getDiffraction() = 0;
	virtual const glm::vec3& getLight() = 0;
	virtual void setColouring(Ray& ray) = 0;
	virtual const std::vector<Ray> getColouring() = 0;
	virtual ~Intersection();
};
}
