#pragma once
#include "Entity.hpp"
#include <vector>

class GeometryNode;

class GeometryEntity : public ltx::Entity{
	static std::vector<GeometryEntity*> entities;
	public:
	GeometryEntity(int id, GeometryNode& node);
	GeometryNode& source;
	virtual ltx::Intersection* intersect(const ltx::Ray& ray);
	virtual const glm::vec3& getSpecular();
	virtual const glm::vec3& getDiffuse();
	static std::vector<GeometryEntity*>& getEntities();
	~GeometryEntity();
};
