#include <cmath>
#include "epsilon.hpp"

float epsilon(float e){
	return e>0? e:0.000001f;
}

float abs(float v){
	return v>0? v: -v;
}

bool lllf::isZero(float v, float e){
	e = epsilon(e);
	return abs(v) < e;
}

bool lllf::equals(float v1, float v2, float e){
	e = epsilon(e);
	float vd = v1 - v2;
	return isZero(vd, e);
}

bool lllf::signCheck(float v, float s, float e){
	int vs = v < 0 ? -1 : 1;
	int ss = s < 0 ? -1 : 1;
	
	return vs*ss > 0 | isZero(v, e);
}

float lllf::lessThan(float v1, float v2, float e){
	e = epsilon(e);
	
	float vd = v2 - v1;
	
	if(isZero(vd, e))
		return 0;

	return vd;
}

float lllf::greaterThan(float v1, float v2, float e){
	return lessThan(v2, v1, e);
}
