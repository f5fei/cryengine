#pragma once
#include "intersect.hpp"
#include "LightSource.hpp"

namespace ltx{

class lightSect : public intersect {
	glm::vec3 radiance;
	public:
	lightSect(float t, LightSource& light, const glm::vec3& radiance);
	virtual const glm::vec3& getLight();
};

}
