#include "Light.hpp"
#include "pointLight.hpp"
#include <glm/gtc/type_ptr.hpp>
#include "vectorOp.hpp"

using namespace glm;

int pointLight::lightId = 0;

std::vector<ltx::LightSource*> pointLight::lights = std::vector<ltx::LightSource*>();

pointLight::pointLight(Light& source):ltx::LightSource(lightId++, source.position, source.colour), source(source){}


//TODO:properly implement point light
glm::vec3 pointLight::getLightCast(const glm::vec3& position){
	float dist = length(position - source.position);

	float mult = 0;
	float term = 0;

	for(int i=0; i<3; i++){
		term += source.falloff[i] * mult;
		mult*=dist;
	}

	return source.colour * (1.0f / term) ;
}

std::vector<ltx::LightSource*>& pointLight::getLightSources(){
	return lights;
}

void pointLight::registerLight(Light& source){
	pointLight* pl = new pointLight(source);
	lights.push_back(pl);
}

pointLight::~pointLight(){}
