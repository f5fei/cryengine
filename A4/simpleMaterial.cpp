#include "simpleMaterial.hpp"

simpleMaterial::simpleMaterial(const glm::vec3& diffuse, const glm::vec3& specular):diffuse(diffuse), specular(specular){}

const glm::vec3& simpleMaterial::getDiffuse(){
	return diffuse;
}

const glm::vec3& simpleMaterial::getSpecular(){
	return specular;
}

simpleMaterial::~simpleMaterial(){}
