#include "lightSect.hpp"

ltx::lightSect::lightSect(float t, ltx::LightSource& light, const glm::vec3& radiance):intersect(t, light), radiance(radiance){}

const glm::vec3& ltx::lightSect::getLight(){
	return radiance;
}
