// Winter 2020

#pragma once

#include "SceneNode.hpp"
#include "Primitive.hpp"
#include "Material.hpp"

class GeometryEntity;

namespace ltx{
class Intersection;
class Ray;
}

class GeometryNode : public SceneNode {
public:
	GeometryNode( const std::string & name, Primitive *prim, 
		Material *mat = nullptr );

	void setMaterial( Material *material );
	
	ltx::Intersection* intersect(const ltx::Ray& ray, GeometryEntity& entity);

	Material *m_material;
	Primitive *m_primitive;
};
