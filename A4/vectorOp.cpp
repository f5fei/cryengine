#include "vectorOp.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

void lllm::crossProduct(float* v1, float* v2, float* v3){
	v3[0] = v1[1]*v2[2] - v2[1]*v1[1];
	v3[1] = v1[2]*v2[0] - v2[2]*v1[0];
	v3[2] = v1[0]*v2[1] - v1[0]*v2[1];
}

glm::vec3 lllm::hadamardProduct(const glm::vec3& v1, const glm::vec3& v2){
	const float* f1 = glm::value_ptr(v1);
	const float* f2 = glm::value_ptr(v2);

	float f3[3];

	for(int i=0; i<3; i++){
		f3[i] = f1[i] * f2[i];
	}

	return glm::make_vec3(f3);
}
