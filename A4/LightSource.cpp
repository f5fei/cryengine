#include "LightSource.hpp"
#include "lightSect.hpp"
#include "epsilon.hpp"
#include "Ray.hpp"
#include "debugging.hpp"

using namespace glm;

ltx::LightSource::LightSource(int id, const glm::vec3& origin, const glm::vec3& color):Entity(id, origin), Radiance(color){
}

glm::vec3 ltx::LightSource::getLightCast(const glm::vec3& position) const {
	return Radiance;
}

//TODO: support raycasting and global illumination
const std::vector<ltx::Ray> ltx::LightSource::sampleCastRays(int mult) const {
	return std::vector<ltx::Ray>();
}

ltx::Intersection* ltx::LightSource::intersect(const ltx::Ray& ray) {
	vec3 dist = Position - ray.getSource();
	vec3 nd = normalize(dist);
	float v = dot(nd, ray.getDirection());
	
	if(lllf::equals(v, 1)){
	//compute intersection data now
		float t = dot(dist, ray.getDirection());
		return new lightSect{t, *this, Radiance};
	} else {
		return nullptr;
	}
}

ltx::LightSource::~LightSource(){}
