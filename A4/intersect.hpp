#pragma once
#include "Intersection.hpp"


namespace ltx{

class Entity;

class intersect : public Intersection{
	Entity& entity;
	glm::vec3 disabled;
	public:
	intersect(float t, Entity& obj);
	virtual int getEntityId();
	virtual const glm::vec3& getNormal();
	virtual const glm::vec3& getPosition();
	virtual const glm::vec3& getDiffusion();
	virtual const glm::vec3& getSpecular();
	virtual const glm::vec3& getTransparency();
	virtual const glm::vec3& getDiffraction();
	virtual const glm::vec3& getLight();
	virtual void setColouring(Ray& ray);
	virtual const std::vector<Ray> getColouring();
	virtual ~intersect();
};
}
