// Winter 2020

#pragma once

#include <glm/glm.hpp>

class Material {
public:	
	virtual const glm::vec3& getDiffuse() = 0;
	virtual const glm::vec3& getSpecular() = 0;
	virtual ~Material();

protected:
	Material();
};
