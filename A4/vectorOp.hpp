/*
Lennox's Library of Linear Mathematical Operations
*/
#pragma once

#include <glm/glm.hpp>

namespace lllm{
	void crossProduct(float* v1, float* v2, float* v3);
	glm::vec3 hadamardProduct(const glm::vec3& v1, const glm::vec3& v2);
}
