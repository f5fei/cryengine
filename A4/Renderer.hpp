#pragma once

#include <glm/glm.hpp>
#include <vector>

namespace ltx{
class Ray;
class Scene;

class Renderer{
	public:
	//the threshold where raytracers will stop tracing
	float threshold;
	//combined with threshold, a less intrusive way of implementing light bounce limit
	float specularCost;
	float diffuseCost;

	float incidenceThreshold;
	float ambientLightThreshold;

	glm::vec3 ambientLight;

	Renderer();

	//forward raytracing for light propagation
	void rayCast(Ray& light, Scene& scene);
	//light source exclusion rules
	//if you want rayCasting and rayTracing together, this must be the rule
	//glm::vec3 rayTrace(Ray& probe, Scene& scene, const std::vector<glm::vec3>& exclusion);
	//take out the exclusion list, temporarily
	glm::vec3 rayTrace(Ray& probe, Scene& scene);

};
}
