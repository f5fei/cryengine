#pragma once
#include <glm/glm.hpp>

struct basis{
	glm::vec3 lookFrom;
	glm::vec3 lookAt;
	glm::vec3 frame;
	glm::vec3 dx;
	glm::vec3 dy;
};

basis produceBasis(glm::vec3 lookFrom, glm::vec3 view, glm::vec3 up, double yfov, int w, int h);
