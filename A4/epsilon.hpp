#pragma once

//floating point precision related calculations
namespace lllf{

	bool isZero(float v, float e = -1);
	bool equals(float v1, float v2, float e = -1);
	bool signCheck(float v, float s, float e = -1);

	float lessThan(float v1, float v2, float e = -1);
	float greaterThan(float v1, float v2, float e = -1);
}
