Compilation Information:

Developed and tested on gl29.student.cs.uwaterloo.ca

To complie, place A0 in a cs488 folder containing the shared libraries. Then inside cs488
$ premake4 gmake
$ make

Then,
$ cd A0
$ make
$ ./A0

This will run and execute the program.

Manual:
Assumptions:
As the constructors assigned hardcoded values for transform, size, rotation and color, the reset function will use the same hardcoded values.

Scale change increments "smoothly" through multiplication.

For the rotation, we use floats from 0 to pi, relying on radian notations.