#include "debugging.hpp"

#include <glm/gtc/type_ptr.hpp>
using namespace glm;

void printVec(float* vec, int dim){
    for(int i=0; i<dim; i++){
        cout<<vec[i]<<"\t";
    }
    cout<<endl;
}


void printMat(float* mat, int dim){
    cout<<"Danger: Actual Matrix"<<endl;
    for(int i=0; i<4; i++){
        for(int j=0; j<4; j++){
            cout<<mat[i*4+j]<<"\t";
        }
        cout<<endl;
    }
    cout<<"---------------------------"<<endl;
}

void printVec3(vec3 vec){
    float* f = value_ptr(vec);
    printVec(f, 3);
}

void printVec4(vec4 vec){
    float* f = value_ptr(vec);
    printVec(f, 4);
}

void printMat4(mat4 mat){
    float* f = value_ptr(mat);
    printMat(f, 4);
}