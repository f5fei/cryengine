// Winter 2020

#include "GeometryNode.hpp"

std::vector<GeometryNode*> GeometryNode::registry;

int GeometryNode::GNodeCount = 0;

//---------------------------------------------------------------------------------------
GeometryNode::GeometryNode(
		const std::string & meshId,
		const std::string & name
)
	: SceneNode(name),
	  meshId(meshId)
{
	m_nodeType = NodeType::GeometryNode;
	registry.push_back(this);
	GNId = GNodeCount++;
}
