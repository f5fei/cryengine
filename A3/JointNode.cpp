// Winter 2020
#include "JointNode.hpp"
#include <glm/gtx/transform.hpp>
#include "cs488-framework/MathUtils.hpp"
#include "debugging.hpp"

std::list<JointNode*> JointNode::registry;

//---------------------------------------------------------------------------------------
JointNode::JointNode(const std::string& name)
	: SceneNode(name)
{
	m_nodeType = NodeType::JointNode;
	registry.push_back(this);
}

//---------------------------------------------------------------------------------------
JointNode::~JointNode() {

}
 //---------------------------------------------------------------------------------------
void JointNode::set_joint_x(double min, double init, double max) {
	m_joint_x.min = min;
	m_joint_x.init = init;
	m_joint_x.max = max;
	xr = 0;
	rotate('x', init);
}

//---------------------------------------------------------------------------------------
void JointNode::set_joint_y(double min, double init, double max) {
	m_joint_y.min = min;
	m_joint_y.init = init;
	m_joint_y.max = max;
	yr = 0;
	rotate('y', init);
}

void JointNode::rotate(char axis, float angle){
	glm::vec3 rot_axis;
	
	switch (axis) {
		case 'x':
			if(xr + angle > m_joint_x.max){		
				angle = m_joint_x.max - xr;
				xr = m_joint_x.max;
			} else if (xr + angle < m_joint_x.min){
				angle = m_joint_x.min - xr;
				xr = m_joint_x.min;
			} else {
				xr += angle;
			}
			rot_axis = glm::vec3(1,0,0);
			break;
		case 'y':
			if(yr + angle > m_joint_y.max){
				angle = m_joint_y.max - yr;
				yr = m_joint_y.max;
			} else if (yr + angle < m_joint_y.min){
				angle = m_joint_y.min - yr;
				yr = m_joint_y.min;
			} else {
				yr += angle;
			}
			rot_axis = glm::vec3(0,1,0);
	        break;
		case 'z':
			//assumption: z axis rotations are still legal
			rot_axis = glm::vec3(0,0,1);
	        break;
		default:
			break;
	}

	rot_axis = glm::vec3(invScale * trans * glm::vec4(rot_axis, 0));

	float rad = degreesToRadians(angle);
	
	glm::vec3 pos = glm::vec3(trans * glm::vec4(0, 0, 0, 1));

	glm::mat4 poT = glm::translate(pos);
	glm::mat4 prT = glm::translate(pos * -1.0f);
	
	glm::mat4 rot_matrix = glm::rotate(rad, rot_axis);
	glm::mat4 rev_matrix = glm::rotate(-rad, rot_axis);
	trans = poT * rot_matrix * prT * trans;
	invtrans = invtrans * poT * rev_matrix * prT;

}
