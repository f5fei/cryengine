// Winter 2020

#include "SceneNode.hpp"

#include "cs488-framework/MathUtils.hpp"

#include <iostream>
#include <sstream>
using namespace std;

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace glm;

#include "debugging.hpp"

// Static class variable
unsigned int SceneNode::nodeInstanceCount = 0;


//---------------------------------------------------------------------------------------
SceneNode::SceneNode(const std::string& name)
  : m_name(name),
	m_nodeType(NodeType::SceneNode),
	trans(mat4(1)),
	invtrans(mat4(1)),
	invScale(mat4(1)),
	isSelected(false),
	m_nodeId(nodeInstanceCount++),
	parent(nullptr)
{

}

//---------------------------------------------------------------------------------------
// Deep copy
SceneNode::SceneNode(const SceneNode & other)
	: m_nodeType(other.m_nodeType),
	  m_name(other.m_name),
	  trans(other.trans),
	  invtrans(other.invtrans),
	  invScale(other.invScale),
	  parent(other.parent)
{
	for(SceneNode * child : other.children) {
		SceneNode* clone = new SceneNode(*child);
		add_child(clone);
	}
}

//---------------------------------------------------------------------------------------
SceneNode::~SceneNode() {
	for(SceneNode * child : children) {
		delete child;
	}
}

//---------------------------------------------------------------------------------------
void SceneNode::set_transform(const glm::mat4& m) {
	trans = m;
	invtrans = inverse(m);
}

//---------------------------------------------------------------------------------------
const glm::mat4& SceneNode::get_transform() const {
	return trans;
}

//---------------------------------------------------------------------------------------
const glm::mat4& SceneNode::get_inverse() const {
	return invtrans;
}

//---------------------------------------------------------------------------------------
void SceneNode::add_child(SceneNode* child) {
	children.push_back(child);
	child->parent = this;
}

//---------------------------------------------------------------------------------------
void SceneNode::remove_child(SceneNode* child) {
	child->parent = NULL;
	children.remove(child);
}

//---------------------------------------------------------------------------------------
void SceneNode::rotate(char axis, float angle) {
	vec3 rot_axis;

	switch (axis) {
		case 'x':
			rot_axis = vec3(1,0,0);
			break;
		case 'y':
			rot_axis = vec3(0,1,0);
	        break;
		case 'z':
			rot_axis = vec3(0,0,1);
	        break;
		default:
			break;
	}

	rot_axis = vec3(invScale * trans * vec4(rot_axis, 0));

	vec3 pos = vec3(trans * vec4(0, 0, 0, 1));

	mat4 poT = glm::translate(pos);
	mat4 prT = glm::translate(pos * -1.0f);
	
	//printMat4(invScale);
	//printMat4(trans);

	//printVec3(rot_axis);

	float rad = degreesToRadians(angle);

	mat4 rot_matrix = glm::rotate(rad, rot_axis);
	mat4 rev_matrix = glm::rotate(-rad, rot_axis);
	trans = poT * rot_matrix  * prT * trans;
	invtrans = invtrans * poT * rev_matrix * prT;
}

//---------------------------------------------------------------------------------------
void SceneNode::scale(const glm::vec3 & amount) {
	mat4 scaler = glm::scale(amount);
	const float* ra = glm::value_ptr(amount);
	float rr[3];
	for(int i=0; i<3; i++){
		rr[i] = 1/ra[i];
	}
	
	vec3 pos = vec3(trans * vec4(0, 0, 0, 1));
	mat4 poT = glm::translate(pos);
	mat4 prT = glm::translate(pos * -1.0f);

	vec3 ram = make_vec3(rr);
	mat4 iscaler = glm::scale(ram);
	trans = poT * scaler * prT * trans;
	invtrans = invtrans * poT * iscaler * prT;
	invScale = invScale * poT * iscaler * prT;
}

//---------------------------------------------------------------------------------------
void SceneNode::translate(const glm::vec3& amount) {
	mat4 translate = glm::translate(amount);
	mat4 invTranslate = glm::translate(amount * -1.0f);
	trans = translate * trans;
	invtrans = invtrans * invTranslate;
}


//---------------------------------------------------------------------------------------
int SceneNode::totalSceneNodes() const {
	return nodeInstanceCount;
}

//---------------------------------------------------------------------------------------
std::ostream & operator << (std::ostream & os, const SceneNode & node) {

	//os << "SceneNode:[NodeType: ___, name: ____, id: ____, isSelected: ____, transform: ____"
	switch (node.m_nodeType) {
		case NodeType::SceneNode:
			os << "SceneNode";
			break;
		case NodeType::GeometryNode:
			os << "GeometryNode";
			break;
		case NodeType::JointNode:
			os << "JointNode";
			break;
	}
	os << ":[";

	os << "name:" << node.m_name << ", ";
	os << "id:" << node.m_nodeId;
	os << "]";

	return os;
}

//--------------------------------------------------------------------------------------
//A top down propagation of all accumulated geometry transformations
//Should be more efficient
void SceneNode::computeTransformations(){
	//cout<<m_name<<endl;
	mat4 prologue = mat4(1.0);
	if(parent){
		prologue = (parent->propagatedCache) * prologue;
	}

	transCache = prologue * trans;
	propagatedCache = prologue * invScale * trans;
	//printMat4(transCache);
	
	for(SceneNode* node : children){
		node->computeTransformations();
	}
}

//-------------------------------------------------------------------------------
void SceneNode::picked(){
	//rigid body moitions always stops at the Joints
	if(m_nodeType == NodeType::JointNode || !parent){
		togglePick();
	} else {
		//this is used for debugging local picking, please use parent->picked() for hierarchy in prod
		//togglePick();
		parent->picked();
    }
}

void SceneNode::togglePick(){
	isSelected = !isSelected;
	//cout<<"Toggled to "<<isSelected<<" for "<<m_nodeId<<endl;
	for(SceneNode* child : children){
		if(child->m_nodeType != NodeType::JointNode){
			child->togglePick();
		}
	}
}
