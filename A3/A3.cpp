// Winter 2020

#include "A3.hpp"
#include "scene_lua.hpp"
using namespace std;

#include "cs488-framework/GlErrorCheck.hpp"
#include "cs488-framework/MathUtils.hpp"
#include "GeometryNode.hpp"
#include "JointNode.hpp"

#include <imgui/imgui.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;
#include <cmath>

#include "TrackBall.hpp"

#include "debugging.hpp"

static bool show_gui = true;

const size_t CIRCLE_PTS = 48;

//----------------------------------------------------------------------------------------
// Constructor
A3::A3(const std::string & luaSceneFile)
	: m_luaSceneFile(luaSceneFile),
	  m_positionAttribLocation(0),
	  m_normalAttribLocation(0),
	  m_vao_meshData(0),
	  m_vbo_vertexPositions(0),
	  m_vbo_vertexNormals(0),
	  m_vao_arcCircle(0),
	  m_vbo_arcCircle(0)
{

}

//----------------------------------------------------------------------------------------
// Destructor
A3::~A3()
{

}

//----------------------------------------------------------------------------------------
/*
 * Called once, at program start.
 */
void A3::init()
{
	// Set the background colour.
	glClearColor(0.85, 0.85, 0.85, 1.0);

	createShaderProgram();

	glGenVertexArrays(1, &m_vao_arcCircle);
	glGenVertexArrays(1, &m_vao_meshData);
	glGenVertexArrays(1, &m_vao_picking);
	enableVertexShaderInputSlots();

	processLuaSceneFile(m_luaSceneFile);

	// Load and decode all .obj files at once here.  You may add additional .obj files to
	// this list in order to support rendering additional mesh types.  All vertex
	// positions, and normals will be extracted and stored within the MeshConsolidator
	// class.
	unique_ptr<MeshConsolidator> meshConsolidator (new MeshConsolidator{
			getAssetFilePath("cube.obj"),
			getAssetFilePath("sphere.obj"),
			getAssetFilePath("suzanne.obj"),
			getAssetFilePath("Geralt.obj")
	});


	// Acquire the BatchInfoMap from the MeshConsolidator.
	meshConsolidator->getBatchInfoMap(m_batchInfoMap);

	// Take all vertex data within the MeshConsolidator and upload it to VBOs on the GPU.
	uploadVertexDataToVbos(*meshConsolidator);

	mapVboDataToVertexShaderInputLocations();

	initPerspectiveMatrix();

	initViewMatrix();

	initLightSources();

	// Exiting the current scope calls delete automatically on meshConsolidator freeing
	// all vertex data resources.  This is fine since we already copied this data to
	// VBOs on the GPU.  We have no use for storing vertex data on the CPU side beyond
	// this point.

	globalTransform = 1;
	displayTrackBall = true;
	zBuffer = true;
	renderMode = 0;
	
	lx = 0;
	ly = 0;

	for(int i=0; i<3; i++){
		clicked[i] = false;
		mouseDown[i] = false;
		released[i] = false;
	}

	vec4 o0 = vec4(0, 0, 0, 1);
	op = vec3(m_rootNode->get_transform() * o0);

}

//----------------------------------------------------------------------------------------
void A3::processLuaSceneFile(const std::string & filename) {
	// This version of the code treats the Lua file as an Asset,
	// so that you'd launch the program with just the filename
	// of a puppet in the Assets/ directory.
	// std::string assetFilePath = getAssetFilePath(filename.c_str());
	// m_rootNode = std::shared_ptr<SceneNode>(import_lua(assetFilePath));

	// This version of the code treats the main program argument
	// as a straightforward pathname.
	m_rootNode = std::shared_ptr<SceneNode>(import_lua(filename));
	if (!m_rootNode) {
		std::cerr << "Could Not Open " << filename << std::endl;
	}
}

//----------------------------------------------------------------------------------------
void A3::createShaderProgram()
{
	m_shader.generateProgramObject();
	m_shader.attachVertexShader( getAssetFilePath("Phong.vs").c_str() );
	m_shader.attachFragmentShader( getAssetFilePath("Phong.fs").c_str() );
	m_shader.link();

	m_shader_arcCircle.generateProgramObject();
	m_shader_arcCircle.attachVertexShader( getAssetFilePath("arc_VertexShader.vs").c_str() );
	m_shader_arcCircle.attachFragmentShader( getAssetFilePath("arc_FragmentShader.fs").c_str() );
	m_shader_arcCircle.link();


	m_shader_picking.generateProgramObject();
	m_shader_picking.attachVertexShader( getAssetFilePath("pick_VertexShader.vs").c_str() );
	m_shader_picking.attachFragmentShader( getAssetFilePath("pick_FragmentShader.fs").c_str() );
	m_shader_picking.link();
}

//----------------------------------------------------------------------------------------
void A3::enableVertexShaderInputSlots()
{
	//-- Enable input slots for m_vao_meshData:
	{
		glBindVertexArray(m_vao_meshData);

		// Enable the vertex shader attribute location for "position" when rendering.
		m_positionAttribLocation = m_shader.getAttribLocation("position");
		glEnableVertexAttribArray(m_positionAttribLocation);

		// Enable the vertex shader attribute location for "normal" when rendering.
		m_normalAttribLocation = m_shader.getAttribLocation("normal");
		glEnableVertexAttribArray(m_normalAttribLocation);

		CHECK_GL_ERRORS;
	}

	{
		glBindVertexArray(m_vao_picking);

		// Enable the vertex shader attribute location for "position" when rendering.
		m_ppAttr = m_shader_picking.getAttribLocation("position");
		glEnableVertexAttribArray(m_ppAttr);

		CHECK_GL_ERRORS;
	}


	//-- Enable input slots for m_vao_arcCircle:
	{
		glBindVertexArray(m_vao_arcCircle);

		// Enable the vertex shader attribute location for "position" when rendering.
		m_arc_positionAttribLocation = m_shader_arcCircle.getAttribLocation("position");
		glEnableVertexAttribArray(m_arc_positionAttribLocation);

		CHECK_GL_ERRORS;
	}

	// Restore defaults
	glBindVertexArray(0);
}

//----------------------------------------------------------------------------------------
void A3::uploadVertexDataToVbos (
		const MeshConsolidator & meshConsolidator
) {
	// Generate VBO to store all vertex position data
	{
		glGenBuffers(1, &m_vbo_vertexPositions);

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexPositions);

		glBufferData(GL_ARRAY_BUFFER, meshConsolidator.getNumVertexPositionBytes(),
				meshConsolidator.getVertexPositionDataPtr(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CHECK_GL_ERRORS;
	}

	// Generate VBO to store all vertex normal data
	{
		glGenBuffers(1, &m_vbo_vertexNormals);

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexNormals);

		glBufferData(GL_ARRAY_BUFFER, meshConsolidator.getNumVertexNormalBytes(),
				meshConsolidator.getVertexNormalDataPtr(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CHECK_GL_ERRORS;
	}

	// Generate VBO to store the trackball circle.
	{
		glGenBuffers( 1, &m_vbo_arcCircle );
		glBindBuffer( GL_ARRAY_BUFFER, m_vbo_arcCircle );

		float *pts = new float[ 2 * CIRCLE_PTS ];
		for( size_t idx = 0; idx < CIRCLE_PTS; ++idx ) {
			float ang = 2.0 * M_PI * float(idx) / CIRCLE_PTS;
			pts[2*idx] = cos( ang );
			pts[2*idx+1] = sin( ang );
		}

		glBufferData(GL_ARRAY_BUFFER, 2*CIRCLE_PTS*sizeof(float), pts, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CHECK_GL_ERRORS;
	}
}

//----------------------------------------------------------------------------------------
void A3::mapVboDataToVertexShaderInputLocations()
{
	// Bind VAO in order to record the data mapping.
	glBindVertexArray(m_vao_meshData);

	// Tell GL how to map data from the vertex buffer "m_vbo_vertexPositions" into the
	// "position" vertex attribute location for any bound vertex shader program.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexPositions);
	glVertexAttribPointer(m_positionAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	// Tell GL how to map data from the vertex buffer "m_vbo_vertexNormals" into the
	// "normal" vertex attribute location for any bound vertex shader program.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexNormals);
	glVertexAttribPointer(m_normalAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	//-- Unbind target, and restore default values:
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	CHECK_GL_ERRORS;

	// Bind VAO in order to record the data mapping.
	glBindVertexArray(m_vao_arcCircle);

	// Tell GL how to map data from the vertex buffer "m_vbo_arcCircle" into the
	// "position" vertex attribute location for any bound vertex shader program.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_arcCircle);
	glVertexAttribPointer(m_arc_positionAttribLocation, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	//-- Unbind target, and restore default values:
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	CHECK_GL_ERRORS;

	glBindVertexArray(m_vao_picking);
	
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexPositions);
	CHECK_GL_ERRORS;

	glVertexAttribPointer(m_ppAttr, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	
	CHECK_GL_ERRORS;

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
void A3::initPerspectiveMatrix()
{
	float aspect = ((float)m_windowWidth) / m_windowHeight;
	m_perspective = glm::perspective(degreesToRadians(60.0f), aspect, 0.1f, 100.0f);
}


//----------------------------------------------------------------------------------------
void A3::initViewMatrix() {
	m_view = glm::lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, -1.0f),
			vec3(0.0f, 1.0f, 0.0f));
}

//----------------------------------------------------------------------------------------
void A3::initLightSources() {
	// World-space position
	m_light.position = vec3(10.0f, 10.0f, 10.0f);
	m_light.rgbIntensity = vec3(0.8f); // light
}

//----------------------------------------------------------------------------------------
void A3::uploadCommonSceneUniforms() {
	m_shader.enable();
	{
		//-- Set Perpsective matrix uniform for the scene:
		GLint location = m_shader.getUniformLocation("Perspective");
		glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(m_perspective));
		CHECK_GL_ERRORS;


		//-- Set LightSource uniform for the scene:
		{
			location = m_shader.getUniformLocation("light.position");
			glUniform3fv(location, 1, value_ptr(m_light.position));
			location = m_shader.getUniformLocation("light.rgbIntensity");
			glUniform3fv(location, 1, value_ptr(m_light.rgbIntensity));
			CHECK_GL_ERRORS;
		}

		//-- Set background light ambient intensity
		{
			location = m_shader.getUniformLocation("ambientIntensity");
			vec3 ambientIntensity(0.25f);
			glUniform3fv(location, 1, value_ptr(ambientIntensity));
			CHECK_GL_ERRORS;
		}
	}
	m_shader.disable();


	m_shader_picking.enable();
	{
		{
			GLint location = m_shader_picking.getUniformLocation("Perspective");
			glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(m_perspective));

			CHECK_GL_ERRORS;
		}

	}
	m_shader_picking.disable();
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, before guiLogic().
 */
void A3::appLogic()
{
	// Place per frame, application logic here ...

	uploadCommonSceneUniforms();

	handleEventUpdates();
	//cout<<"App Tick!"<<endl;

}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after appLogic(), but before the draw() method.
 */
void A3::guiLogic()
{
	if( !show_gui ) {
		return;
	}

	static bool firstRun(true);
	if (firstRun) {
		ImGui::SetNextWindowPos(ImVec2(50, 50));
		firstRun = false;
	}

	static bool showDebugWindow(true);
	ImGuiWindowFlags windowFlags(ImGuiWindowFlags_AlwaysAutoResize);
	float opacity(0.5f);

	ImGui::Begin("Properties", &showDebugWindow, ImVec2(100,100), opacity,
			windowFlags);
	
		if( ImGui::RadioButton("Position/Orientation", &globalTransform, 1)){
		}
		ImGui::SameLine();
		if( ImGui::RadioButton("Joints", &globalTransform, 0)){
		}
		
		if(ImGui::TreeNode("Application")){

			if( ImGui::Button( "Reset Position" ) ) {
				doResetPosition();
			}
			ImGui::SameLine();
			if( ImGui::Button( "Reset Orientation" ) ) {
				doResetOrientation();
			}
			ImGui::SameLine();
			if( ImGui::Button( "Reset Joints" ) ) {
				doResetJoints();
			}
			ImGui::SameLine();
			if( ImGui::Button( "Reset All" ) ) {
				doResetAll();
			}
			ImGui::SameLine();
			// Create Button, and check if it was clicked:
			if( ImGui::Button( "Quit" ) ) {
				doQuit();
			}
			ImGui::TreePop();
		}
		if(ImGui::TreeNode("Edit")){
			if(ImGui::Button("Undo")){
				doundo();
			}
			ImGui::SameLine();
			if(ImGui::Button("Redo")){
				doredo();
			}
			ImGui::TreePop();
		}

		if(ImGui::TreeNode("Options")){
			if( ImGui::Checkbox("Circle", &displayTrackBall)){
			}
			ImGui::SameLine();
			if( ImGui::Checkbox("Z-buffer", &zBuffer)){
			}
			if( ImGui::RadioButton("Backface culling", &renderMode, 1)){
			}
			ImGui::SameLine();
			if( ImGui::RadioButton("Frontface culling", &renderMode, -1)){
			}
			ImGui::SameLine();
			if( ImGui::RadioButton("No culling", &renderMode, 0)){
			}
			ImGui::TreePop();
		}

		ImGui::Text( "Framerate: %.1f FPS", ImGui::GetIO().Framerate );

	ImGui::End();
}

//----------------------------------------------------------------------------------------
// Update mesh specific shader uniforms:
static void updateShaderUniforms(
		const ShaderProgram & shader,
		const GeometryNode & node,
		const glm::mat4 & viewMatrix
) {

	shader.enable();
	{
		//-- Set ModelView matrix:
		GLint location = shader.getUniformLocation("ModelView");
		mat4 modelView = viewMatrix * node.transCache;
		glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(modelView));
		CHECK_GL_ERRORS;

		//-- Set NormMatrix:
		location = shader.getUniformLocation("NormalMatrix");
		mat3 normalMatrix = glm::transpose(glm::inverse(mat3(modelView)));
		glUniformMatrix3fv(location, 1, GL_FALSE, value_ptr(normalMatrix));
		CHECK_GL_ERRORS;


		//-- Set Material values:		
		location = shader.getUniformLocation("material.kd");

		vec3 kd = node.material.kd;
		if(node.isSelected){
			kd = kd * -0.4f + vec3(1);
		}
		glUniform3fv(location, 1, value_ptr(kd));
		CHECK_GL_ERRORS;
	}
	shader.disable();

}

static void updateShaderUniformsPicking(
	const ShaderProgram& shader,
	const GeometryNode & node,
	const glm::mat4& viewMatrix
){

	shader.enable();
	{
		GLint location = shader.getUniformLocation("ModelView");
		mat4 modelView = viewMatrix * node.transCache;
		glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(modelView));

		int id = node.GNId;

        location = shader.getUniformLocation("col");

		vec3 col = vec3((id>>16)%256, (id>>8)%256, id%256);

		col = col * (1.0f / 256.0f);

		glUniform3fv(location, 1, value_ptr(col));
		CHECK_GL_ERRORS;

	}
	shader.disable();
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after guiLogic().
 */
void A3::draw() {

	if(zBuffer)
		glEnable( GL_DEPTH_TEST );
	
	if(renderMode){
		glEnable(GL_CULL_FACE);
		if(renderMode == -1){
			glCullFace(GL_FRONT);
		} else if(renderMode == 1){
			glCullFace(GL_BACK);
		}
	}

	renderSceneGraph(*m_rootNode);
	//renderPicking(*m_rootNode);


	glDisable( GL_DEPTH_TEST );
	glDisable(GL_CULL_FACE);
	
	if(displayTrackBall)
		renderArcCircle();
}

//----------------------------------------------------------------------------------------
void A3::renderSceneGraph(SceneNode & root) {

	// Bind the VAO once here, and reuse for all GeometryNode rendering below.
	glBindVertexArray(m_vao_meshData);

	// This is emphatically *not* how you should be drawing the scene graph in
	// your final implementation.  This is a non-hierarchical demonstration
	// in which we assume that there is a list of GeometryNodes living directly
	// underneath the root node, and that we can draw them in a loop.  It's
	// just enough to demonstrate how to get geometry and materials out of
	// a GeometryNode and onto the screen.

	// You'll want to turn this into recursive code that walks over the tree.
	// You can do that by putting a method in SceneNode, overridden in its
	// subclasses, that renders the subtree rooted at every node.  Or you
	// could put a set of mutually recursive functions in this class, which
	// walk down the tree from nodes of different types.


	/*
	for (const SceneNode * node : root.children) {

		if (node->m_nodeType != NodeType::GeometryNode)
			continue;

		const GeometryNode * geometryNode = static_cast<const GeometryNode *>(node);

		updateshaderuniforms(m_shader, *geometrynode, m_view);


		// get the batchinfo corresponding to the geometrynode's unique meshid.
		batchinfo batchinfo = m_batchinfomap[geometrynode->meshid];

		//-- now render the mesh:
		m_shader.enable();
		gldrawarrays(gl_triangles, batchinfo.startindex, batchinfo.numindices);
		m_shader.disable();
	}*/

	root.computeTransformations();

	for (const GeometryNode* node : GeometryNode::registry){
		updateShaderUniforms(m_shader, *node, m_view);
		// get the batchinfo corresponding to the geometrynode's unique meshid.
		BatchInfo batchInfo = m_batchInfoMap[node->meshId];
		//-- now render the mesh:
		m_shader.enable();
		glDrawArrays(GL_TRIANGLES, batchInfo.startIndex, batchInfo.numIndices);
		m_shader.disable();
	}

	glBindVertexArray(0);
	CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
// Draw the trackball circle.
void A3::renderArcCircle() {
	glBindVertexArray(m_vao_arcCircle);

	m_shader_arcCircle.enable();
		GLint m_location = m_shader_arcCircle.getUniformLocation( "M" );
		float aspect = float(m_framebufferWidth)/float(m_framebufferHeight);
		glm::mat4 M;
		if( aspect > 1.0 ) {
			M = glm::scale( glm::mat4(), glm::vec3( 0.5/aspect, 0.5, 1.0 ) );
		} else {
			M = glm::scale( glm::mat4(), glm::vec3( 0.5, 0.5*aspect, 1.0 ) );
		}
		glUniformMatrix4fv( m_location, 1, GL_FALSE, value_ptr( M ) );
		glDrawArrays( GL_LINE_LOOP, 0, CIRCLE_PTS );
	m_shader_arcCircle.disable();

	glBindVertexArray(0);
	CHECK_GL_ERRORS;
}

//-----------------------------------------------------------------------------------------

void A3::renderPicking(SceneNode& root){
	root.computeTransformations();

	glBindVertexArray(m_vao_picking);

	for(const GeometryNode* gnode : GeometryNode::registry){
		updateShaderUniformsPicking(m_shader_picking, *gnode, m_view);

		BatchInfo batchInfo = m_batchInfoMap[gnode->meshId];
		//-- now render the mesh:
		m_shader_picking.enable();
		glDrawArrays(GL_TRIANGLES, batchInfo.startIndex, batchInfo.numIndices);
		m_shader_picking.disable();
	}

	glBindVertexArray(0);
}

//----------------------------------------------------------------------------------------
/*
 * Called once, after program is signaled to terminate.
 */
void A3::cleanup()
{

}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles cursor entering the window area events.
 */
bool A3::cursorEnterWindowEvent (
		int entered
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse cursor movement events.
 */
bool A3::mouseMoveEvent (
		double xPos,
		double yPos
) {
	bool eventHandled(false);
	//cout<<"Move event"<<endl;

	// Fill in with event handling code...
	
	xPos *= double(m_framebufferWidth) / double(m_windowWidth);
	yPos = m_windowHeight - yPos;
	yPos *= double(m_framebufferWidth) / double(m_windowWidth);
		
	cx = xPos;
	cy = yPos;

	eventHandled = true;

	//cout<<cx<<"\t"<<cy<<endl;
	//cout<<lx<<"\t"<<ly<<endl;
	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse button events.
 */
bool A3::mouseButtonInputEvent (
		int button,
		int actions,
		int mods
) {
	bool eventHandled(false);

	int index = 0;

	switch (button){
		case GLFW_MOUSE_BUTTON_LEFT:
			index = 0;
			break;
		case GLFW_MOUSE_BUTTON_MIDDLE:
			index = 1;
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
 			index = 2;
 			break;
		default:
			index = 0;
			break;
	}

	if(actions == GLFW_PRESS) {
		clicked[index] = true;
		mouseDown[index] = true;

		double xpos, ypos;

		glfwGetCursorPos( m_window, &xpos, &ypos );

		xpos *= double(m_framebufferWidth) / double(m_windowWidth);
		ypos = m_windowHeight - ypos;
		ypos *= double(m_framebufferWidth) / double(m_windowWidth);
		
		cx = xpos;
		cy = ypos;

	} else if(actions == GLFW_RELEASE) {
		released[index] = true;
		mouseDown[index] = false;	
	}
	//cout<<"Input event fired"<<endl;
	eventHandled = true;

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse scroll wheel events.
 */
bool A3::mouseScrollEvent (
		double xOffSet,
		double yOffSet
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles window resize events.
 */
bool A3::windowResizeEvent (
		int width,
		int height
) {
	bool eventHandled(false);
	initPerspectiveMatrix();
	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles key input events.
 */
bool A3::keyInputEvent (
		int key,
		int action,
		int mods
) {
	bool eventHandled(false);

	if( action == GLFW_PRESS ) {
		eventHandled = true;
		if( key == GLFW_KEY_M ) {
			show_gui = !show_gui;
		} else if( key == GLFW_KEY_C ) {
			displayTrackBall = !displayTrackBall;
		} else if(key == GLFW_KEY_Z){
			zBuffer = !zBuffer;
		} else if(key == GLFW_KEY_B){
			renderMode = 1;
		} else if(key == GLFW_KEY_F){
			renderMode = -1;
		} else if (key == GLFW_KEY_P){
			globalTransform = 1;
		} else if (key == GLFW_KEY_J){
			globalTransform = 0;
		} else if(key == GLFW_KEY_I){
			doResetPosition();
		} else if(key == GLFW_KEY_O){
			doResetOrientation();
		} else if(key == GLFW_KEY_S){
			doResetJoints();
		} else if(key == GLFW_KEY_A){
			doResetAll();
		} else if(key == GLFW_KEY_Q){
			doQuit();
		} else if(key == GLFW_KEY_R){
			doredo();
		} else if(key == GLFW_KEY_U){
			doundo();
		}
	}
	// Fill in with event handling code...

	return eventHandled;
}

//---------------------------------------------------------------------------------
void A3::performPicking(){
	//picking
	uploadCommonSceneUniforms();
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.35, 0.35, 0.35, 1.0);

	renderPicking(*m_rootNode);

	CHECK_GL_ERRORS;

	GLubyte buffer[4] = {0, 0, 0, 0};
	glReadBuffer( GL_BACK );
	glReadPixels(int(cx), int(cy), 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

	CHECK_GL_ERRORS;

	unsigned int id = (buffer[0]<<16) + (buffer[1]<<8) + buffer[2];

	if(id<GeometryNode::GNodeCount){
		GeometryNode* picked = GeometryNode::registry[id];
		picked->picked();
	}
	glClearColor(0.85, 0.85, 0.85, 1.0);
}

void A3::performManip(){
	
	float ccx = pixelToNorm(cx, 'x');
	float ccy = pixelToNorm(cy, 'y');
	float llx = pixelToNorm(lx, 'x');
	float lly = pixelToNorm(ly, 'y');
	
	//trackball
	if(mouseDown[2]){
		float r[3];
		vCalcRotVec(ccx, ccy, llx, lly, 1.0, r);
		mat4 rot = vAxisRotMatrix(r);
		mat4 ort = m_rootNode->get_transform();
		vec3 pos = vec3(ort * vec4(0,0,0,1));
		mat4 preT = translate(pos * -1.0f);
		mat4 posT = translate(pos);
		mat4 nt = posT * rot * preT * ort;
		m_rootNode->set_transform(nt);
	}

	//panning
	if(mouseDown[0]){
		vec3 amount = vec3(ccx - llx, ccy - lly, 0);
		//scaling this because the current version feels sluggish
		mat4 t = translate(amount * 7.2f);
		mat4 ort = m_rootNode->get_transform();
		mat4 nt = t * ort;
		m_rootNode->set_transform(nt);
	}

	if(mouseDown[1]){
		float dx = ccx - llx;
		float dy = ccy - lly;
		float dz = sqrt(dx*dx + dy*dy);
		if(dx + dy < 0){
			dz*=-1.0f;
		}
		vec3 amount = vec3(0, 0, dz);
		mat4 t = translate(amount);
		mat4 ort = m_rootNode->get_transform();
		mat4 nt = t * ort;
		m_rootNode->set_transform(nt);
	}


}

void A3::performLocal(){
	if(clicked[0]){
		performPicking();
	}

	float ccx = pixelToNorm(cx, 'x');
	float ccy = pixelToNorm(cy, 'y');
	float llx = pixelToNorm(lx, 'x');
	float lly = pixelToNorm(ly, 'y');	

	float dx = ccx - llx;
	float dy = ccy - lly;

	//angular change
	if(mouseDown[1]){
		records rec;

		//only exemption of joint movement
		float xr = dy * 180 / 3.141592653;
		float yr = dx * 180 / 3.141592653;

		rec.x = xr;
		rec.y = yr;

		for(JointNode* node : JointNode::registry){
			if(node->isSelected){
				node->rotate('x', xr);
				node->rotate('y', yr);
				rec.joints.push_back(node);
			}
		}
		undo.push_back(rec);
	}


	if(mouseDown[2]){
		for(JointNode* node : JointNode::registry){
			//considering the amount of details in this, this is truly the only accessible way to do this even if it is quite hacky
			if(node->m_name == "neck"){
				if(node->isSelected){
					float dz = sqrt(dx*dx + dy*dy) * 180 / 3.141592653;
					if(dx + dy < 0){
						dz *= -1;
					}
					node->rotate('z', dz);
				}
				break;
			}
		}
	}

}


void A3::handleEventUpdates(){
	// a bit like the interrupt handling in trains
	//clicked, released are discrete signals, they should only be acknowledge once

	if(globalTransform){
		//literally does not care about clicked or released signals, only cares about mouse down
		performManip();
	} else {
		performLocal();
	}


	//mouseDown is a level signal so we don't touch it

	for(int i=0; i<3; i++){
		clicked[i] = false;
		released[i] = false;
	}

	//cout<<"Updating legacy values"<<endl;
	lx = cx;
	ly = cy;
}

float A3::pixelToNorm(float v, char dim){
	float divisor = dim == 'x'? m_framebufferWidth : m_framebufferHeight;
	return v * 2.0/divisor - 1.0;
}

void A3::doundo(){
	
	if(undo.empty())
		return;

	records rec = undo.back();
	for(JointNode* node : rec.joints){
		node->rotate('x', -rec.x);
		node->rotate('y', -rec.y);
	}
	redo.push_back(rec);
	undo.pop_back();
}


void A3::doredo(){
	
	if(redo.empty())
		return;

	records rec = redo.back();
	for(JointNode* node : rec.joints){
		node->rotate('x', rec.x);
		node->rotate('y', rec.y);
	}
	undo.push_back(rec);
	redo.pop_back();
}

//at some point you simply wonder, if this truely necessary?

void A3::doResetPosition(){
	mat4 trans = m_rootNode->get_transform();
	vec4 o = vec4(0, 0, 0, 1);
	vec4 pp = trans * o;
	vec3 dif = op - vec3(pp);
	mat4 t = translate(dif);
	trans = t * trans;
	m_rootNode->set_transform(trans);
}

void A3::doResetOrientation(){
	vec4 dir = vec4(1, 0, 0, 0);
	vec4 pos = vec4(0, 0, 0, 1);
	mat4 trans = m_rootNode->get_transform();

	vec4 npos = trans * pos;
	vec4 nd = m_rootNode->invScale * trans * dir;

	mat4 preT = translate(vec3(pos - npos));
	mat4 posT = translate(vec3(npos - pos));

	vec3 rotVec = cross(vec3(dir), vec3(nd));
	vec3 dd = vec3(dir - nd);
	float* ddd = value_ptr(dd);

	float sin = 0;
	for(int i=0; i<3; i++){
		sin+=ddd[i] * ddd[i];
	}
	sin = sqrt(sin);
	float rad = asin(sin/2)*2;


	if(rad>0.0001){
		mat4 rm = rotate(mat4(1), -rad, rotVec);
		trans = posT * rm * preT * trans;
	}
	m_rootNode->set_transform(trans);
}

void A3::doResetJoints(){
	float xd, yd;
	for(JointNode* node : JointNode::registry){
		xd = node->m_joint_x.init - node->xr;
		yd = node->m_joint_y.init - node->yr;

		node->rotate('x', xd);
		node->rotate('y', yd);
	}

	undo.clear();
	redo.clear();
}

void A3::doResetAll(){
	doResetJoints();
	doResetPosition();
	doResetOrientation();
}


void A3::doQuit(){
	glfwSetWindowShouldClose(m_window, GL_TRUE);
}
