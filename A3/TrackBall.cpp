#include "TrackBall.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "cmath"
using namespace std;

#include "debugging.hpp"


void normalizeVec(float x, float y, float d, float* fv){
	fv[0] = x * 2.0 / d;
	fv[1] = y * 2.0 / d;
	fv[2] = 1.0 - fv[0]*fv[0] - fv[1]*fv[1];

	float fl;
	if(fv[2] < 0.0){
		fl = sqrt(1.0 - fv[2]);
		fv[2] = 0.0;
		fv[0] /= fl;
		fv[1] /= fl;
	} else {
		fv[2] = sqrt(fv[2]);
	}
}

void vCalcRotVec(float cx, float cy, float lx, float ly, float dia, float* rotVec){
	float fo[3], fn[3];

	normalizeVec(cx, cy, dia, fn);
	normalizeVec(lx, ly, dia, fo);
	
	rotVec[0] = fo[1] * fn[2] - fn[1] * fo[2];
	rotVec[1] = fo[2] * fn[0] - fn[2] * fo[0];
	rotVec[2] = fo[0] * fn[1] - fn[0] * fo[1];
}

glm::mat4 vAxisRotMatrix(float* rotVec){
	float rad = 0;
	for(int i=0; i<3; i++){
		rad += rotVec[i] * rotVec[i];
	}
	rad = sqrt(rad);
	using namespace glm;
	
	vec3 rv = make_vec3(rotVec);
	rv = rv * (float)(1.0/rad);
	
	mat4 m = mat4(1);
	if(rad > 0.000001){
		m = rotate(m, rad, rv);
	}
	return m;
}
