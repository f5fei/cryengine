// Winter 2020

#pragma once

#include <vector>
#include "SceneNode.hpp"

class GeometryNode : public SceneNode {
public:
	GeometryNode(
		const std::string & meshId,
		const std::string & name
	);

	Material material;

	int GNId;

	// Mesh Identifier. This must correspond to an object name of
	// a loaded .obj file.
	std::string meshId;

	static std::vector<GeometryNode*> registry;
	static int GNodeCount;
};
