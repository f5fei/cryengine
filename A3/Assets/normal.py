
def computeNormal(v):
	n = [0, 0, 0]
	v1 = v[0] - v[1]
	v2 = v[1] - v[2]
	n[0] = v1[1] * v2[2] - v2[1] - v1[2];
	n[1] = v1[2] * v2[0] - v2[2] - v1[0];
	n[2] = v1[0] * v2[1] - v2[0] - v1[1];
	return n;



def inputLoop():

	vsd = dict()
	while 1:
		p = input("Vertice: k v1 v2 v3\n")
		if p[0] == 'q':
			break
		ns = p.split()
		k = ns[0]
		v = list(map(lambda s : float(s), ns[1:]))
		print(k, v)
		vsd[k] = v

	while 1:
		p = input("Polygon: k1 k2 k3\n")
		if p[0] == 'q':
			break
		ns = p.split()[:3]
		n = computeNormal(list(map(lambda s : vsd[s], ns)))
		print(n)


inputLoop()
