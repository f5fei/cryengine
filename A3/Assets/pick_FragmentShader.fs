#version 330

uniform vec3 col;

out vec4 fragColour;

void main() {
	fragColour = vec4(col, 1);
}
