-- Powered by Nvidia HairWorks TM

root = gr.node('root')

skin = gr.material({1.0, 0.8, 0.7}, {0.15, 0.15, 0.15}, 1);
hair = gr.material({0.8, 0.8, 0.8}, {0.5, 0.5, 0.5}, 10);
armor = gr.material({0.1, 0.1, 0.15}, {0.2, 0.2, 0.2}, 1);
maile = gr.material({0.4, 0.4, 0.4}, {0.8, 0.8, 0.8}, 10);
leather = gr.material({0.3, 0.2, 0.1}, {0.4, 0.4, 0.4}, 4);
blade = gr.material({0.96, 0.96, 1}, {1, 1, 1}, 10);

head = gr.mesh('Geralt', 'head');
head:set_material(skin)

scalp = gr.mesh('Geralt', 'scalp')
scalp:set_material(hair)
scalp:translate(0, 0.1, -0.1)
head:add_child(scalp)

head:rotate('x', -90)

body = gr.node('body')

collar = gr.node('collar')
neck = gr.joint('neck', {-45, 0, 45}, {-45, 0, 45})
collar:rotate('x', 90)


neck:add_child(head)

collar:add_child(neck)

body:add_child(collar)

root:add_child(body)

chest = gr.mesh('cube', 'chest');
chest:scale(12, 6, 6)
chest:translate(0, -4, 0)
chest:set_material(armor)
body:add_child(chest)

waist = gr.mesh('cube', 'waist')
waist:scale(9, 13, 5)
waist:translate(0, -11, 0)
waist:set_material(leather)
body:add_child(waist)


chain = gr.mesh('cube', 'maile')
chain:scale(5, 12, 5.25)
chain:translate(0, -10, 0)
chain:set_material(maile)
body:add_child(chain)

ss1 = gr.node('shoulder pads')
ss1:rotate('y', 90)
sh1 = gr.joint('shoulder', {-60, 0, 110}, {-110, 0, 20})
ss1:add_child(sh1)
ss1:translate(6,-3,0);
ar1 = gr.mesh('cube', 'arm')
ar1:scale(3, 3, 10)
ar1:translate(0, 0, 5);
ar1:set_material(maile)
sh1:add_child(ar1)
el1 = gr.joint('elbow', {0, 0, 0}, {-90, 0, 0})
el1:translate(0, 0, 10)
ah1 = gr.mesh('cube', 'arm-hand')
ah1:scale(3, 3, 10)
ah1:translate(0, 0, 5)
ah1:set_material(leather)
el1:add_child(ah1)
ar1:add_child(el1)

ss2 = gr.node('shoulder pads')
ss2:rotate('y', -90)
sh2 = gr.joint('shoulder', {-60, 0, 110}, {-20, 0, 110})
ss2:translate(-6,-3,0);
ss2:add_child(sh2)
ar2 = gr.mesh('cube', 'arm')
ar2:scale(3, 3, 10)
ar2:translate(0, 0, 5);
ar2:set_material(maile)
sh2:add_child(ar2)
el2 = gr.joint('elbow', {0, 0, 0}, {0, 0, 90})
el2:translate(0, 0, 10)
ah2 = gr.mesh('cube', 'arm-hand')
ah2:scale(3, 3, 10)
ah2:translate(0, 0, 5)
ah2:set_material(leather)
el2:add_child(ah2)
ar2:add_child(el2)

w = gr.node('wrist')
w:translate(0, 0, 10)
w:rotate('y', 45)
sh = gr.joint('sword hand', {-90, 0, 90}, {-90, 0, 90})
s = gr.mesh('cube', 'sword')
s:scale(1, 0.4, 32)
s:translate(0, 0, 16)
s:set_material(blade)
sh:add_child(s)
w:add_child(sh)
ah2:add_child(w)


body:add_child(ss1)
body:add_child(ss2)


pl1 = gr.node('leg_base');
pl1:translate(4, -14, 0)
pl1:rotate('x', 90)
ps1 = gr.joint('pelvic socket', {-90, 0, 45}, {-40, 0, 80})
lt1 = gr.mesh('cube', 'leg')
lt1:scale(4, 4, 16);
lt1:translate(0, 0, 8);
lt1:set_material(armor);
pl1:add_child(ps1);
ps1:add_child(lt1);
body:add_child(pl1);
lk1 = gr.joint('knee socket', {0, 0, 90}, {0,0,0})
lk1:translate(0, 0, 16)
ll1 = gr.mesh('cube', 'lower leg');
ll1:scale(4, 4, 12);
ll1:translate(0, 0, 6);
ll1:set_material(armor);
lk1:add_child(ll1)
lt1:add_child(lk1)

body:add_child(pl1)

pl2 = gr.node('leg_base');
pl2:translate(-4, -14, 0)
pl2:rotate('x', 90)
ps2 = gr.joint('pelvic socket', {-90, 0, 45}, {-80, 0, 40})
lt2 = gr.mesh('cube', 'leg')
lt2:scale(4, 4, 16);
lt2:translate(0, 0, 8);
lt2:set_material(armor);
pl2:add_child(ps2);
ps2:add_child(lt2);
body:add_child(pl2);
lk2 = gr.joint('knee socket', {0, 0, 90}, {0,0,0})
lk2:translate(0, 0, 16)
ll2 = gr.mesh('cube', 'lower leg');
ll2:scale(4, 4, 12);
ll2:translate(0, 0, 6);
ll2:set_material(armor);
lk2:add_child(ll2)
lt2:add_child(lk2)

body:add_child(pl1)

hj1_1 = gr.joint('hair socket', {-30, -10, 30}, {-30, 0, 30});
hj1_1:translate(0, 7, -3)
hs1_1 = gr.mesh('cube', 'hair segment');
hs1_1:scale(0.8, 0.8, -1.6)
hs1_1:set_material(hair)
hs1_1:translate(0, 0, -0.8)
hj1_1:add_child(hs1_1)

hj1_2 = gr.joint('hair socket', {-30, -30, 30}, {-30, 0, 30});
hj1_2:translate(0, 0, -1.6)
hs1_2 = gr.mesh('cube', 'hair segment');
hs1_2:scale(1.2, 1.2, -1.6)
hs1_2:set_material(hair)
hs1_2:translate(0, 0, -0.8)
hj1_2:add_child(hs1_2);
hs1_1:add_child(hj1_2);

hj1_3 = gr.joint('hair socket', {-30, -20, 30}, {-30, 0, 30});
hj1_3:translate(0, 0, -1.6)
hs1_3 = gr.mesh('cube', 'hair segment');
hs1_3:scale(1.4, 1.4, -1.6)
hs1_3:set_material(hair)
hs1_3:translate(0, 0, -0.8)
hj1_3:add_child(hs1_3)
hs1_2:add_child(hj1_3)


hj1_4 = gr.joint('hair socket', {-30, -15, 30}, {-30, 0, 30});
hj1_4:translate(0, 0, -1.6)
hs1_4 = gr.mesh('cube', 'hair segment');
hs1_4:scale(1.3, 1.3, -1.6)
hs1_4:set_material(hair)
hs1_4:translate(0, 0, -0.8)
hj1_4:add_child(hs1_4)
hs1_3:add_child(hj1_4)

hj1_5 = gr.joint('hair socket', {-30, -15, 30}, {-30, 0, 30});
hj1_5:translate(0, 0, -1.6)
hs1_5 = gr.mesh('cube', 'hair segment');
hs1_5:scale(1.2, 1.2, -1.6)
hs1_5:set_material(hair)
hs1_5:translate(0, 0, -0.8)
hj1_5:add_child(hs1_5)
hs1_4:add_child(hj1_5)


hj1_6 = gr.joint('hair socket', {-30, -15, 30}, {-30, 0, 30});
hj1_6:translate(0, 0, -1.6)
hs1_6 = gr.mesh('cube', 'hair segment');
hs1_6:scale(1.0, 1.0, -1.6)
hs1_6:set_material(hair)
hs1_6:translate(0, 0, -0.8)
hj1_6:add_child(hs1_6)
hs1_5:add_child(hj1_6)


head:add_child(hj1_1);



root:translate(0, 0, -10)

return root;
