#pragma once

#include <glm/glm.hpp>

void vCalcRotVec(float cx, float cy, float lx, float ly, float diam, float* rotVec);

glm::mat4 vAxisRotMatrix(float* rotVec);
