#pragma once

#include "lModel.hpp"

//makes many assumptions about the shader layout
//in exchange, this can be adopted and applied for most situations
//streamlining the process at the cost of a bit of performance loss
class standardModel : public lModel {
    public:
    standardModel(float* vertice, int* indice, int verticeCount, int polyCount, ShaderProgram& shader);
    virtual void configureShaderData(bool closure = true);
    virtual void updateInstance();
    virtual void updateUniform(glm::mat4& P, glm::mat4& V);
    virtual ~standardModel(); 


    protected:
    GLint P_uni;
    GLint V_uni;
    GLuint inst_vbo;

}
