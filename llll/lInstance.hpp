#pragma once

//llll -> v1.0

#include <glm/glm.hpp>

class lModel;

class lInstance{
    public:
    lInstance(lModel& prototype, glm::mat4& M); 
    int getId() const;
    glm::mat4 getModelMatrix() const;
    //TODO: apply more transformations

    protected:
    lModel& prototype;
    //model transformation matrix
    glm::mat4 M;

    private:
    int id;
};
