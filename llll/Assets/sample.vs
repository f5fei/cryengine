#version 330
#note, this will be more complete when texture is implemented

uniform mat4 P;
uniform mat4 V;

in vec3 pos;
in mat4 M;

void main(){
    gl_position = P * V * M * vec4(pos, 1.0);
}
