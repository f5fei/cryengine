#include "lModel.hpp"
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/ShaderProgram.hpp"

lModel::lModel(float* vertice, int* indice, int verticeCount, int polyCount, ShaderProgram& shader):instanceCount(0), verticeCount(verticeCount), polyCount(polyCount), shader(shader) {
    id = ++mId;
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float)*verticeCount*3, vertice, GL_STREAM_DRAW);
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY, ebo);
    glBufferData(GL_ELEMENT_ARRAY, sizeof(int)*polyCount*3, indice, GL_STREAM_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY, 0);
}

void lModel::configureShaderdata(bool closure){
    glBindVertexArray(vao);
    glBindBuffer( GL_ARRAY_BUFFER, vbo );
    glBindBuffer( GL_ELEMENT_ARRAY, ebo );

    if(closure){
	glBindVertexArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY, 0);
    }
}

int lModel::registerInstance(lInstance& instance){
    instances.push_back(&instance);
    return ++instanceCount;
}

void lModel::draw(){
    shader.enable();
    glBindVertexArray(vao);	
    glDrawElementsInstanced(GL_TRIANGLES, polyCount*3, GL_UNSIGNED_INT, 0, instanceCount);
    shader.disable();
}

int lModel::getId(){
    return id;
}

lModel::~lModel(){
    glDeleteBuffers(1, ebo);
    glDeleteBuffers(1, vbo);
    glDeleteVertexArrays(1, vao);
}

void lModel::initialization(){
    mId = 0;
}

//TODO:implement concrete standard model
