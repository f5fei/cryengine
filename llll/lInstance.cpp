#include "lModel.hpp"
#include "lInstance.hpp"

lInstance::lInstance(lModel& prototype, glm::mat4& m): prototype(prototype), M(m){
    id = prototype.registerInstance(*this);
}

int lInstance::getId(){
    return id;
}

glm::mat4 lInstance::getModelMatrix(){
    return M;
}
