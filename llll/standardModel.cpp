#include "lInstance.hpp"
#include "standardModel.hpp"

standardModel::standardModel(float* vertice, int* indice, int verticeCount, int polyCount, ShaderProgram& shader): lModel(vertice, indice, verticeCount, polyCount, shader){
    glGenBuffer(1, $inst_vbo);
    P_uni = shader.getUniformLocation("P");
    V_uni = shader.getUniformLocation("V");
}

void standardModel::configureShaderData(bool closure){
    //How to combine setup without overriding vertex array object
    lModel::configureShaderData(false);

    GLint posAttr = shader.getAttribLocation("pos");
    GLint mAttr = shader.getAttribLocation("M");
    
    glEnableVertexAttribArray(posAttr);
    glVertexAttribPointer( posAttr, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float)*verticeCount, nullptr);
    glBindBuffer( GL_ARRAY_BUFFER, inst_vbo );
    for(int i=0; i<4; i++){
	glEnableVertexAttribArray(mAttr + i);
	glVertexAttribPointer(mAttr+i, 4, GL_FLOAT, GL_FALSE, 16*sizeof(float), (void*)(i*16));
    }

    if(closure){
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY, 0);
    }
}

void standardModel::updateInstance(){
    float instanceData = new float[ 16*instanceCount ];
    for(int i=0; i<instanceCount; i++){
	lInstance* inst = instances[i];
	const float* m = (const float*)glm::value_ptr(inst->M);
	for(int j=0; j<16; i++){
	    instanceData[16*i + j] = m[j];
	}
    }
    glBindBuffer( GL_ARRAY_BUFFER, inst_vbo ); 
    glBufferData( GL_ARRAY_BUFFER, instanceCount*16*sizeof(float), instanceData, GL_DYNAMIC_DRAW );
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    delete [] instanceData;
}

void standardModel::updateUniform(glm::mat4& P, glm::mat4& V){
    shader.enable();
    glUniformMatrix4v( P_uni, 1, GL_FALSE, glm::value_ptr( P ) );
    glUniformMatrix4v( V_uni, 1, GL_FALSE, glm::value_ptr( V ) );
    shader.disable();
}

standardModel::~standardModel(){
    glDeleteVertexArray(vao);
    glDeleteBuffer(vbo);
    glDeleteBuffer(ebo);
    glDeleteBuffer(inst_vbo);
}
