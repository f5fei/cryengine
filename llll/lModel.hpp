#pragma once

//llll -> v1.0

#include <vector>
#include <glm/glm.hpp>

// The super generic version that allows for full customization

// We will have a more well implmented generic version that we expect to be used for most scenarios

//while this is not super portable, switching this out for the openGL shaderProgram isn't that difficult

class ShaderProgram;
class lInstance;

class lModel{
    public:
    //input: vertice: array of 3-tuples
    lModel(float* vertice, int* indice, int verticeCount, int polyCount, ShaderProgram& shader);
    //registers with the parent Model, returns a Model unique id
    virtual int registerInstance(lInstance& instance);
    //set this up to avoid coupling
    virtual void configureShaderData(bool closure = true);
    virtual void updateInstance() = 0;
    virtual void draw();
    int getId() const;
    virtual ~lModel();
    //may seem pointless now, but we might use this to construct lookup tables and such
    static void initialization();
    //TODO: implement texture and normalmap

    ShaderProgram& shader;

    protected:
    static int mId;
    std::vector<lInstance*> instances;
    int instanceCount;
    int verticeCount;
    int polyCount;
    GLuint vao;
    GLuint vbo;
    GLuint ebo;
    
    private:
    int id;

};
